!function($){
    $(function(){
        /**
         * Sending msg
         */
        $('#sendingMsg ').on('submit', function () {
            var idUserTo = $(this).closest('.chat').find('.users input[name=user]:checked').closest('li').data('id');
            var textArea = $(this).closest('form').find('textarea');

            if( idUserTo ){
                jQuery.ajax({
                    type: 'GET',
                    url: '/chat/send_message?id_user_to=' + idUserTo + '&' + 'text=' + textArea.val(),
                    dataType: 'json',
                    success: function(data){
                        if( data == 'success'){
                            textArea.val('');
                        }

                        return false;
                    },
                    error: function(data){
                        console.log('Error sending message...');
                        return false;
                    }
                });

                //for on-line users functional
                jQuery.ajax({
                    type: "POST",
                    url: '/user/add_activity'
                });
            }else{
                alert('Выберите получателя!');
            }

            return false;
        });

        //send msg after pressing enter
        $("#sendingMsg textarea").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
                $(this).closest('form').submit();
            }
        });
        /** End
         * Sending msg
         */

        /**
         * Shows chat between two users
         */
        $('input[name=user]').on('change', function(){
            var idUserTo = $(this).closest('ul').find('input[name=user]:checked').closest('li').data('id');
            if( idUserTo ){
                jQuery.ajax({
                    type: 'GET',
                    url: '/chat/get_chat?id_user_to=' + idUserTo,
                    dataType: 'json',
                    success: function(data){
                        setMessages(data, idUserTo);
                    },
                    error: function(data){
                        console.log('Error sending message...');
                    }
                });

                //for on-line users functional
                jQuery.ajax({
                    type: "POST",
                    url: '/user/add_activity'
                });
            }else{
                alert('Выберите получателя!');
            }
        });
        /** End
         * Shows chat between two users
         */

        /**
         *  Each 1 sec update msgs
         */
        setInterval(function() {
            var idUserTo;
            if( idUserTo = $('.users input[name=user]:checked').closest('li').data('id') ){
                jQuery.ajax({
                    type: "POST",
                    url: '/chat/get_chat?id_user_to=' + idUserTo,
                    dataType: 'json',
                    success: function (data) {
                        setMessages(data, idUserTo);
                        // window.scrollTo(0, 100);
                    }
                });
            }

        }, 1000);

        function setMessages(data, idUserTo) {
            var str = '';

            for(var i = 0; i < data.length; i++) {
                str += '<div class="msgContainer ';
                if( data[i].id_user_to == idUserTo ){
                    str += 'msgTo';
                }else{
                    str += 'msgFrom';
                }
                str += '">';
                str += '<span class="email_from">' + data[i].email_from + '</span><br/>';
                str += '<span class="msg">' + data[i].text + '</span><br/>';
                str += data[i].created_at + '<br/><br/>';
                str += '</div>';
            }

            $('.messages').html(str);
        }
        /** End
         *  Each 1 sec update msgs
         */

        /**
         * Uploading
         */
        $('#uploadform-file').on('change', function (event) {
            var idUserTo = $(' .users input[name=user]:checked').closest('li').data('id');
            if( idUserTo ){
                $$f({
                    formid:'sendFiles',
                    url:'/chat/files?id_user_to=' + idUserTo,
                    onstart:function () {},
                    onsend:function () {}
                });
            }else{
                alert('Выберите получателя!');
            }
            
            //for on-line users functional
            jQuery.ajax({
                type: "POST",
                url: '/user/add_activity'
            });
        });

        $('#clip').on('click', function () {
            $('#uploadform-file').click();
        });
        /** End
         * Uploading
         */

        /**
         * Show online users
         */
        setInterval(function() {
            jQuery.ajax({
                type: "POST",
                url: '/user/get_users_activities',
                dataType: 'json',
                success: function (data) {
                    var users = $('.users');
                    users.find('li').each(function(){
                        id = $(this).data('id');
                        if(data[id].is_online == 1){
                            $(this).addClass('online')
                        }else{
                            $(this).removeClass('online');
                        }
                    });
                }
            });
        }, 2000);
        /** End
         * Show online users
         */

        /**
         * Show only online users
         */
        $('input[name=showOnlyOnline]').on('change', function () {
            var notOnline = $('.users ul li:not(.online)');
            if( $(this).prop( "checked" ) ){
                notOnline.hide();
            }else{
                notOnline.show();
            }
        });
        /** End
         * Show only online users
         */
    })
}(jQuery);
