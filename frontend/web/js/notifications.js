
!function($){
    $(function(){

        /*
         * Show notifications
         */
        setInterval(function () {
            jQuery.ajax({
                type: "POST",
                url: '/notification/get_notifications',
                dataType: "json",
                success: function(data){

                    var chat = '';
                    var elaboration = '';

                    for(var i = 0; i < data.length; i++) {
                        if( data[i].type == 0 ){// 0 - chat
                            chat += '<div class="notification" data-id="' + data[i].id_notification + '">';
                            chat += data[i].email + ' сообщение ' + data[i].datetime;
                            chat += '</div>';
                        }else{// 1 - elaboration answer
                            elaboration += '<div class="notification" data-id="' + data[i].id_notification + '">';
                            elaboration += data[i].theme + ' сообщение ' + data[i].author;
                            elaboration += '</div>';
                        }
                    }

                    $('.panel-abottom-right .panel-container-vertical').html(chat);
                    $('.panel-abottom-left .panel-container-vertical').html(elaboration);
                },
                error: function(data){
                    console.log('Error get notifications');
                }
            });
        }, 2000);
        /* End
         * Show notifications
         */

        /**
         * Deletes notification after click
         */
        $('body').on('click','.panel-container-vertical .notification', function () {
            jQuery.ajax({
                type: "POST",
                url: '/notification/delete?id=' + $(this).data('id')});
        });
        /** End
         * Deletes notification after click
         */

    })
}(jQuery);
