
!function($){
    $(function(){
        $("#activeTasksPanel").tablesorter();
        $(".myTasks").tablesorter();
        /**
         * Switching through between tasks
         */
        var id_task;
        $('#activeTasksPanel input[name=showDetails]').on('change', function(){
            id_task = parseInt( $(this).parent().parent().data('id') );
            jQuery.ajax({
                type: "POST",
                url: '/task/delete_reader_from_tasks',
                /*success: function () {
                    alert('success' + id_task);
                },
                error: function () {
                    alert('Error adding reader...');
                }*/
            });

            jQuery.ajax({
                type: "POST",
                url: '/task/add_reader_to_the_task?id_task=' + id_task
            });

            //taking task history
            jQuery.ajax({
                type: "GET",
                url: '/history/get_history?id_task=' + id_task,
                dataType: "json",
                success: function(data){
                    // console.log(data);
                    var str = '';
                    for(var i = 0; i < data.length; i++) {
                        switch( data[i].action ){
                            case 0 :{//create
                                str += data[i].created + ' - Aвтор - ' + data[i].userEmail + '<br/>';
                                str += data[i].theme + '<br/>';
                                str += 'Текст: ' + data[i].text + '<br/>';
                                str += data[i].attached + '<br/><br/><br/>';
                                break;
                            }
                            case 1 :{//appoint
                                str += data[i].created + '<br/>';
                                str += data[i].userEmail + '<br/>';
                                str += 'Назначил исполнителя: ' + data[i].taskUserEmail + '<br/><br/><br/>';
                                break;
                            }
                            case 2 :{//abandon
                                str += data[i].created + '<br/>';
                                str += data[i].userEmail + '<br/>';
                                str += 'Отказался от задачи ' + '<br/><br/><br/>';
                                break;
                            }
                            case 3 :{//elaborationQuestion
                                str += data[i].created + '<br/>';
                                str += data[i].userEmail + '<br/>';
                                str += 'Уточнение: вопрос ' + '<br/><br/><br/>';
                                break;
                            }
                            case 4 :{//elaborationAnswer
                                str += data[i].created + '<br/>';
                                str += data[i].taskUserEmail + '<br/>';
                                str += 'Уточнение: ответ ' + '<br/><br/><br/>';
                                break;
                            }
                            case 5 :{//closingTask
                                str += data[i].created + '<br/>';
                                str += data[i].userEmail + '<br/>';
                                str += 'Закрытие задачи ' + '<br/><br/><br/>';
                                break;
                            }
                            default :{
                                break;
                            }
                        }
                    }
                    $('.panel-bottom').html(str);
                },
                error: function(data){
                    console.log('Error query take task history');
                }
            });
            
            //Show details on showTaskDetails panel
            $('.panel-right-top').html($(this).closest('.tr').find('.taskText').html());
            //Show files attached to task
            $('.panel-right-middle').html($(this).closest('.tr').find('.taskFiles').html());

            //for on-line users functional
            jQuery.ajax({
                type: "POST",
                url: '/user/add_activity'
            });
        });
        /** End
         * Switching through between tasks
         */

        /**
         * Update readers after each 3 seconds
         */
        setInterval(function() {
            var status ;
            jQuery.ajax({
                type: "POST",
                url: '/task/get_readers_of_tasks',
                success: function (data) {

                    $('#activeTasksPanel .tr').each(function(){
                        id = $(this).data('id');

                        if(data[id].status == 2){              //2 - прострочена, color - red
                            $(this).addClass('red')
                        }

                        currentDatetime = (Date.now()/1000).toFixed(0);

                        if( data[id].lastReadTime ){
                            differenceInTime =  currentDatetime - data[id].lastReadTime;
                            if(differenceInTime <= 180){                                              //Online less than 180 sec
                                $(this).find('.taskIsReadingNow').html(data[id].userReadingEmail);     //Output readers from DB
                            }else if( data[id].userReadingEmail != null ){
                                $(this).find('.taskIsReadingNow').html(null);     //Output readers from DB
                                jQuery.ajax({
                                    type: "POST",
                                    url: '/task/delete_reader_from_the_task?id_task=' + id
                                });
                            }
                        }
                    });
                }
            });
        }, 3000);  //Update
        /** End
         * Update readers after each 3 seconds
         */

        /**
         * Resizable
         */
        $(".panel-atop-left").resizable({
            handleSelector: ".splitter-vertical-middle",
            resizeHeight: false
        });
        $(".panel-left-top").resizable({
            handleSelector: ".splitter-left-top",
            resizeWidth: false
        });
        $(".panel-left-middle").resizable({
            handleSelector: ".splitter-left-middle",
            resizeWidth: false
        });
        $(".panel-right-middle").resizable({
            handleSelector: ".splitter-right-top",
            resizeWidth: false
        });
        $(".panel-right-top").resizable({
            handleSelector: ".splitter-right-middle",
            resizeWidth: false
        });
        $(".panel-atop").resizable({
            handleSelector: ".splitter-atop",
            resizeWidth: false
        });
        $(".panel-abottom-left").resizable({
            handleSelector: ".splitter-vertical-abottom",
            resizeHeight: false
        });
        $('.panel__trigger').on('click', function(){
            $(this).closest('.split').toggleClass('minimized').next().slideToggle()
        });
        /** End
         * Resizable
         */

        /**
         * Changing color selected task
         */
        $('#activeTasksPanel input[name=showDetails]').on('click', function(){
            $('#activeTasksPanel .tr').each(function(){
                $(this).removeClass('selected');
            });

            $(this).closest('.tr').addClass('selected');
        });
        /** End
         * Changing color selected task
         */

        /**
         * Appoints task for himself
         */
        $('#takeTaskHimself').on('click', function(){
            var radioTasks = $('#activeTasksPanel input[name=showDetails]:checked');
            if(!radioTasks.val()){
                alert('Выберите задачу');
                return false;
            }

            jQuery.ajax({
                type: "POST",
                url: '/task/appoint_task?id_task=' + radioTasks.val(),
                dataType: "json",
                success: function(data){
                    console.log(data);
                    if( data.success ){
                        radioTasks.closest('.tr').hide();
                    }

                    window.location.reload();
                },
                error: function(data){
                    console.log('Error query take task themeself');
                }
            });

            //for on-line users functional
            jQuery.ajax({
                type: "POST",
                url: '/user/add_activity'
            });
        });
        /** End
         * Appoints task for himself
         */

        /**
         * Appoints task for some manager
         */
        $('.appointTask').on('click', function(){
            panel = $(this).closest('.workPanel');
            radioTasks = panel.find('input[name=showDetails]:checked');
            selectManager = panel.find('#managers');
            if(!radioTasks.val()){
                alert('Выберите задачу');
                return false;
            }
            if(!selectManager.val() || selectManager.val() == 0){
                alert('Выберите менеджера');
                return false;
            }

            jQuery.ajax({
                type: "POST",
                url: '/task/appoint_task?id_task=' + radioTasks.val() + '&id_user=' + selectManager.val(),
                dataType: "json",
                success: function(data){
                    if( data.success ){
                        radioTasks.closest('.tr').hide();
                    }
                    location.reload();
                },
                error: function(data){
                    console.log('Error query take task themeself');
                }
            });

            //for on-line users functional
            jQuery.ajax({
                type: "POST",
                url: '/user/add_activity'
            });
        });
        /** End
         * Appoints task for himself
         */

        /**
         * Gets & puts chosen datetime after click button
         */
        //Yesterday
        $('#getAndPasteYesterdayDate').on('click', function(){
            $(this).closest('form').find('#dateFrom').val(
                moment().subtract(1, 'days').format('YYYY-MM-DD 00:00')
            );

            $(this).closest('form').find('#dateTo').val(
                moment().subtract(1, 'days').format('YYYY-MM-DD HH:mm')
            );
            return false;
        });
        //Today
        $('#getAndPasteTodayDate').on('click', function(){
            $(this).closest('form').find('#dateFrom').val(
                moment().format('YYYY-MM-DD 00:00')
            );

            $(this).closest('form').find('#dateTo').val(
                moment().format('YYYY-MM-DD HH:mm')
            );
            return false;
        });
        //Current week
        $('#getAndPasteCurrentWeekDate').on('click', function(){
            $(this).closest('form').find('#dateFrom').val(
                moment().startOf('week').add(1, 'days').format('YYYY-MM-DD 00:00')
            );

            $(this).closest('form').find('#dateTo').val(
                moment().format('YYYY-MM-DD HH:mm')
            );
            return false;
        });
        /** End
         * Gets & puts chosen datetime after click button
         */

        /**
         * Filter by author & status
         */
        $('.filterEmail').on('click', function(event){
            $(event.target).closest('form')[0].submit()
        });
        $('#activeTasksPanel .filterStatus').on('click', function(event){
            $(event.target).closest('form')[0].submit()
        });
        /** End
         * Filter by author & status
         */

        /**
         * Abandoning task
         */
        $('#abandonTask').on('click', function(){
            var checkedTask = $(this).closest('#myTasksPanel').find('input[name=showDetails]:checked');//$('#activeTasksPanel input[name=showDetails]:checked');
            if( checkedTask.val() ){
                window.location.replace('/task/abandon_from_task?id_task=' + checkedTask.val());
            }else{
                alert('Выберите задачу');
                return false;
            }

            //for on-line users functional
            jQuery.ajax({
                type: "POST",
                url: '/user/add_activity'
            });
        });
        /** End
         * Abandoning task
         */

        /**
         * Elaboration form
         */
        $('#clarify').on('click', function(){

            //for on-line users functional
            jQuery.ajax({
                type: "POST",
                url: '/user/add_activity'
            });

            var checkedTask = $(this).closest('#myTasksPanel').find('input[name=showDetails]:checked');//$('#activeTasksPanel input[name=showDetails]:checked');
            if( checkedTask.val() ){

                jQuery.ajax({
                    type: "POST",
                    url: '/task/get_task_in_json?id_task=' + checkedTask.val(),
                    dataType: "json",
                    success: function(data){
                        var formClarify = $('#formClarify');
                        formClarify.find('.recipient').html(data.author);
                        formClarify.find('input[name=recipient]').val(data.author);

                        formClarify.find('.theme').html(data.theme);
                        formClarify.find('input[name=theme]').val(data.theme);

                        $('#formElaborationDialog').html($('.elaborationDialog').html());

                        formClarify.find('input[name=taskId]').val(checkedTask.val());
                        formClarify.find('input[name=recipientId]').val(data.id_author);
                    },
                    error: function(data){
                        console.log('Error query take task himself');
                    }
                });             

            }else{
                alert('Выберите задачу');
                return false;
            }
        });
        /** End
         * Elaboration form
         */

        /**
         * show details after choosing task in my task panel
         */
        $('#myTasksPanel input[name=showDetails]').on('change', function (){
            $('.panel-right-middle').html('');
            var taskId = $(this).closest('.tr').data('id');
            var taskText = $(this).closest('.tr').find('.taskText').html();
            var panelRightTop = $('.panel-right-top');
            var str="";

            jQuery.ajax({
                type: "POST",
                url: '/elaboration/get_elaboration_dialog_in_json?id_task=' + taskId,
                dataType: "json",
                success: function(data){
                    str += '<div class="elaborationDialog">';
                    for(var i = 0; i < data.length; i++) {
                        str += data[i].type + ':' + '<br/>';
                        str += 'От кого: ' + data[i].from + '<br/>';
                        str += data[i].created + '<br/>';
                        str += data[i].text + '<br/>' + '<br/>';
                    }

                    str += taskText + '<br/>' + '<br/>';
                    str += '</div>';

                    panelRightTop.html(str);
                },
                error: function(data){
                    console.log('Error query take task himself');
                }
            });

            //for on-line users functional
            jQuery.ajax({
                type: "POST",
                url: '/user/add_activity'
            });
        });
        /** End
         * show details after choosing task in my task panel
         */

        /**
         * Elaboration form
         */
        $('#closeTask').on('click', function(){

            //for on-line users functional
            jQuery.ajax({
                type: "POST",
                url: '/user/add_activity'
            });
            
            var checkedTask = $(this).closest('#myTasksPanel').find('input[name=showDetails]:checked');//$('#activeTasksPanel input[name=showDetails]:checked');
            if( checkedTask.val() ){

                jQuery.ajax({
                    type: "POST",
                    url: '/task/get_task_in_json?id_task=' + checkedTask.val(),
                    dataType: "json",
                    success: function(data){
                        var formClarify = $('#formCloseTask');
                        formClarify.find('.recipient').html(data.author);
                        formClarify.find('input[name=recipient]').val(data.author);

                        formClarify.find('.theme').html(data.theme);
                        formClarify.find('input[name=theme]').val(data.theme);

                        $('#formCloseTaskElaborationDialog').html($('.elaborationDialog').html());

                        formClarify.find('input[name=taskId]').val(checkedTask.val());
                        formClarify.find('input[name=recipientId]').val(data.id_author);
                    },
                    error: function(data){
                        console.log('Error query take task himself');
                    }
                });

            }else{
                alert('Выберите задачу');
                return false;
            }
        });
        /** End
         * Elaboration form
         */

        /**
         * Shows tasks of user for admin
         */
        $('#adminPanel select').on('change', function () {
            var usersTasks = $('#adminPanel #usersTasks');
            
            jQuery.ajax({
                type: "POST",
                url: '/task/get_user_tasks?id_user=' + $(this).val(),
                dataType: "json",
                success: function(data){
                    if( data['error'] == undefined ){
                        var panel = '<table><tbody>';

                        for(var i = 0; i < data.length; i++) {
                            panel += '<tr>';
                            panel += '<td>' + data[i].author + '</td>';
                            panel += '<td>' + data[i].theme + '</td>';
                            panel += '<td>' + data[i].status + '</td>';
                            panel += '</tr>';
                        }

                        panel += '</tbody></table>';
                        usersTasks.html(panel);
                    }else{
                        usersTasks.html();
                    }
                },
                error: function(data){
                    console.log('Error get user\'s task...');
                }
            });
        })
        /** End
         * Shows tasks of user for admin
         */
    })
}(jQuery);
