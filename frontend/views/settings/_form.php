<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Settings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'upload_dir')->textInput() ?>

    <?= $form->field($model, 'reactionTime')->textInput() ?>

    <?= $form->field($model, 'operation_day_from')->textInput() ?>

    <?= $form->field($model, 'operation_day_to')->textInput() ?>

    <?= $form->field($model, 'inputPost_password')->textInput() ?>

    <?= $form->field($model, 'inputPost_port')->textInput() ?>

    <?= $form->field($model, 'inputPost_email')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
