<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\UserSearch */
/* @var $managersDataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать', ['/site/signup'], ['class' => 'btn btn-success']) ?>
    </p>

    <h2>Менеджера</h2>
    <?= GridView::widget([
        'dataProvider' => $managersDataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'username',
             'email:email',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <h2>Заказчики</h2>
    <?= GridView::widget([
        'dataProvider' => $customersDataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'username',
             'email:email',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
