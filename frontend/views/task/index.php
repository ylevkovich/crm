<?php

use yii\helpers\Html;
use frontend\components\ConvertTime;
use frontend\models\Task;
use frontend\controllers\TaskController;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $todayTasks. Tasks what have status 'new'*/
/* @var $myTasks. Tasks appointed for current user*/
/* @var $filters. Array of names of used filters*/
/* @var $filterPanel. String if myTasks filtered, null if filtered active tasks*/
/* @var $managers. List of users, what have status manager*/
/* @var $modelUploadForm frontend\models\UploadForm */
/* @var $uploadDir Directory from settings table */

$this->title = 'Tasks';
$this->registerCssFile('../css/task/index.css');
$this->registerJsFile('/assets/f99dcc2c/jquery.js');
$this->registerJsFile('/assets/f99dcc2c/resizable.js');
$this->registerJsFile('/assets/f99dcc2c/jquery.tablesorter.min.js');
$this->registerJsFile('/assets/f99dcc2c/moment.js');
$this->registerJsFile('/js/task/index.js');
$this->registerJsFile('/js/notifications.js');
?>

<svg xmlns="http://www.w3.org/2000/svg" width="0" height="0" display="none">
    <symbol id="arrow-down-mini" viewBox="-122.9 121.1 105.9 61.9">
        <path d="M-63.2 180.3l43.5-43.5c1.7-1.7 2.7-4 2.7-6.5s-1-4.8-2.7-6.5c-1.7-1.7-4-2.7-6.5-2.7s-4.8 1-6.5 2.7L-69.9 161l-37.2-37.2c-1.7-1.7-4-2.7-6.5-2.7s-4.8 1-6.5 2.6c-1.9 1.8-2.8 4.2-2.8 6.6 0 2.3.9 4.6 2.6 6.5 11.4 11.5 41 41.2 43 43.3l.2.2c3.6 3.6 10.3 3.6 13.9 0z"/>
    </symbol>
</svg>

<div class="panel-container">

    <?php if( Yii::$app->user->identity->position == \frontend\models\User::$pos['admin']):?>
        <a href="/user">Пользователи</a>
        <a href="/settings/update?id=1">Настройки</a>
    <?php endif;?>

    <div class="panel-atop">
        <div class="panel-left panel-atop-left">
            <div class="panel-container-vertical">
                <div class="split splitter-horizontal">
                    <a href="#" title="Свернуть" class="panel__trigger">
                        <svg class="icon-arrow-down-mini">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow-down-mini"></use>
                        </svg>
                    </a>
                </div>
                <div class="workPanel panel-top panel-left-top">
                    <div class="posa">
                        <div class="buttonsPanel">
                            <button id="takeTaskHimself" type="button" class="panel__btn" title="Назначить выбраную задачу на себя">Взять себе</button>

                            <select id="managers" class="panel__btn">
                                <option value="0">Выберите менеджера</option>
                                <?php foreach ($managers as $key => $manager): ?>
                                    <option value="<?=$manager->id?>">
                                        <?=$manager->email?>
                                    </option>
                                <?php endforeach;?>
                            </select>
                            <button type="button" class="appointTask panel__btn" title="Назначить выбраную задачу на выбраного менеджера">Назначить</button>

                            <?php
                            Modal::begin([
                                'header' => '<h3>Фильтр по дате</h3>',
                                'toggleButton' => [
                                    'label' => '<span class="glyphicon glyphicon-time"></span>',
                                    'class' => 'panel__btn',
                                    'title' => 'Установить период просмотра',
                                    'id' => 'dateFilter',
                                ],
                            ]);
                            ?>

                            <?php $form = ActiveForm::begin([
                                'options' =>[
                                    'enctype' => 'multipart/form-data',
                                    'class' => 'dateFilter'
                                ]
                            ]); ?>
                            <div class="form-group">
                                <label class="control-label" for="dateFrom">От</label>
                                <?= DateTimePicker::widget([
                                    'name' => 'dateFrom',
                                    'id' => 'dateFrom',
                                    'options' => ['placeholder' => 'Select operating time ...'],
                                ]); ?>

                                <div class="help-block"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="dateTo">По</label>
                                <?= DateTimePicker::widget([
                                    'name' => 'dateTo',
                                    'id' => 'dateTo',
                                    'options' => ['placeholder' => 'Select operating time ...'],
                                ]); ?>

                                <div class="help-block"></div>
                            </div>
                            <button id="getAndPasteYesterdayDate" class="btn btn-default">Вчера</button>
                            <button id="getAndPasteTodayDate" class="btn btn-default">Сегодня</button>
                            <button id="getAndPasteCurrentWeekDate" class="btn btn-default">Текущая неделя</button>
                            <div class="form-group">
                                <?= Html::submitButton('Фильтровать', ['class' => 'btn btn-success']) ?>
                            </div>
                            <br/>

                            <?php ActiveForm::end(); ?>

                            <?php
                            Modal::end();
                            ?>
                            <div class="filters">
                                <?php
                                if($filters && !$filterPanel)
                                    foreach ($filters as $filter)
                                        echo '<span class="filterName">'.$filter.'<a href="/task/">x</a></span>';
                                ?>
                            </div>
                        </div>
                        <?php if($todayTasks):?>
                            <table id="activeTasksPanel" class="panel1 panel__table tablesorter">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Иконка</th>
                                    <th>Автор</th>
                                    <th>Тема</th>
                                    <th>Статус</th>
                                    <th>Читает</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($todayTasks as $key => $todayTask): ?>
                                    <tr class="tr <?=Task::$statusTranslate[$todayTask->status]['color']?>" data-id="<?=$todayTask->id?>">
                                        <td class="taskText" style="display: none">
                                            <?=$todayTask->text?>
                                        </td>
                                        <td class="taskFiles" style="display: none">
                                            <ul>
                                                <?php foreach($todayTask->files as $file): ?>
                                                    <li>
                                                        <a href="<?='../'.$uploadDir.'/'.$todayTask->user->email.'/'
                                                        .$todayTask->user->email.'_'.ConvertTime::toDatetime_YmdGis($todayTask->created).'/'
                                                        .ConvertTime::toDatetime_YmdGis($todayTask->created).'_'.$todayTask->id.'/'
                                                        .$file->name?>">
                                                            <?=$file->name?>
                                                        </a>
                                                    </li>
                                                <?php endforeach;?>
                                            </ul>
                                        </td>

                                        <td class="td color__trigger">
                                            <input type="radio" name="showDetails" title="" value="<?=$todayTask->id?>"/>
                                        </td>
                                        <td class="td">
                                        <span class="glyphicon glyphicon-file">
                                        </td>

                                        <td class="td filterEmail">
                                            <form method="POST" action="/task/">
                                                <?=$todayTask->userEmail?>
                                                <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                                                <input type="text" value="<?=$todayTask->userEmail?>" name="filterAuthor" style="display: none"/>
                                            </form>
                                        </td>
                                        <td class="td">
                                            <?=TaskController::generateRightTheme($todayTask)?>
                                        </td>
                                        <td class="td filterStatus">
                                            <form method="POST" action="/task/">
                                                <?=Task::$statusTranslate[$todayTask->status]['rusName']?>
                                                <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                                                <input type="text" value="<?=$todayTask->status?>" name="filterStatus" style="display: none"/>
                                                <input type="text" value="<?=Task::$statusTranslate[$todayTask->status]['rusName']?>" name="filterStatusName" style="display: none"/>
                                            </form>
                                        </td>
                                        <td class="td taskIsReadingNow">
                                            <?=$todayTask->userReadingEmail?>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        <?php endif;?>
                    </div>
                </div>

                <div class="split splitter-horizontal splitter-left-top">
                    <a href="#" title="Свернуть" class="panel__trigger">
                        <svg class="icon-arrow-down-mini">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow-down-mini"></use>
                        </svg>
                    </a>
                </div>

<!--                <div class="panel-middle panel-left-middle">left middle panel</div>-->
<!---->
<!--                <div class="split splitter-horizontal splitter-left-middle">-->
<!--                    <a href="#" title="Свернуть" class="panel__trigger">-->
<!--                        <svg class="icon-arrow-down-mini">-->
<!--                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow-down-mini"></use>-->
<!--                        </svg>-->
<!--                    </a>-->
<!--                </div>-->

                <div class="workPanel ottom">
                    <?php if( Yii::$app->user->identity->position == \frontend\models\User::$pos['admin']):?>
                        <div id="adminPanel" class="panel1 panel__table">
                            <select class="panel__btn">
                                <option value="0">Выберите менеджера</option>
                                <?php foreach ($managers as $key => $manager): ?>
                                    <option value="<?=$manager->id?>">
                                        <?=$manager->email?>
                                    </option>
                                <?php endforeach;?>
                            </select>
                            <div id="usersTasks"></div>
                        </div>
                    <?php else: ?>
                        <div id="myTasksPanel" class="panel1 panel__table">
                            <div class="tabs">
                                <span class="myTasks">Мои задачи</span>
                                <span class="TasksAppointedByMe">Поставлены мною</span>
                            </div>
                            <div class="buttonsPanel">
                                <button id="abandonTask" type="button" class="panel__btn" title="Отказаться от задачи">Отказаться</button>
                                <?php
                                Modal::begin([
                                    'header' => '<h3>Уточнение задачи</h3>',
                                    'toggleButton' => [
                                        'label' => 'Уточнить',
                                        'class' => 'panel__btn',
                                        'title' => 'Уточнить',
                                        'id' => 'clarify',
                                    ],
                                ]);
                                ?>
                                <?php $form = ActiveForm::begin([
                                    'options' =>[
                                        'id' => 'formClarify'
                                    ],
                                    'action' => ['/elaboration']
                                ]); ?>
                                <label>Получатель:
                                    <span class="recipient"></span>
                                </label><br/>
                                <label>Тема:
                                    <span class="theme"></span>
                                </label><br/>
                                <textarea class="text" name="text"></textarea>
                                <div id="formElaborationDialog"></div>
                                <?= $form->field($modelUploadForm, 'file[]')->fileInput(['multiple' => true,'class' => 'btn btn-default']) ?>
                                <input name="recipient" type="hidden"/>
                                <input name="recipientId" type="hidden"/>
                                <input name="taskId" type="hidden"/>
                                <input name="theme" type="hidden"/>
                                <div class="clearfix">
                                    <input type="submit" class="btn btn-success" value="Отправить"/>
                                </div>
                                <?php ActiveForm::end(); ?>
                                <?php Modal::end(); ?>

                                <select id="managers" class="panel__btn">
                                    <option value="0">Выберите менеджера</option>
                                    <?php foreach ($managers as $key => $manager): ?>
                                        <option value="<?=$manager->id?>">
                                            <?=$manager->email?>
                                        </option>
                                    <?php endforeach;?>
                                </select>
                                <button type="button" class="appointTask panel__btn" title="Назначить выбраную задачу на выбраного менеджера">Назначить</button>

                                <?php
                                Modal::begin([
                                    'header' => '<h3>Закрытие задачи</h3>',
                                    'toggleButton' => [
                                        'label' => 'Закрыть задачу',
                                        'class' => 'panel__btn',
                                        'title' => 'Закрыть выполненую задачу',
                                        'id' => 'closeTask',
                                    ],
                                ]);
                                ?>
                                <?php $form = ActiveForm::begin([
                                    'options' =>[
                                        'id' => 'formCloseTask'
                                    ],
                                    'action' => ['/task/close_task']
                                ]); ?>
                                <label>Получатель:
                                    <span class="recipient"></span>
                                </label><br/>
                                <label>Тема:
                                    <span class="theme"></span>
                                </label><br/>

                                <?php CKEditor::begin([
                                    'preset' => 'full',
                                    'name' => 'fusdfll',
                                ]);?>
                                <input type="text" name="text">
                                <?php CKEditor::end();?>

                                <div id="formCloseTaskElaborationDialog"></div>

                                <?= $form->field($modelUploadForm, 'file[]')->fileInput(['multiple' => true,'class' => 'btn btn-default']) ?>
                                <input name="recipient" type="hidden"/>
                                <input name="recipientId" type="hidden"/>
                                <input name="taskId" type="hidden"/>
                                <input name="theme" type="hidden"/>
                                <div class="clearfix">
                                    <input type="submit" class="btn btn-success" value="Отправить"/>
                                </div>
                                <?php ActiveForm::end(); ?>
                                <?php Modal::end(); ?>

                                <a href="/chat" type="button" title="Открыть чат" class="panel__btn" title="Отказаться от задачи">Чат</a>

                                <div class="filters">
                                    <?php
                                    if($filters && $filterPanel)
                                        foreach ($filters as $filter)
                                            echo '<span class="filterName">'.$filter.'<a href="/task/">x</a></span>';
                                    ?>
                                </div>

                            </div>
                            <?php if($myTasks):?>
                                <table class="myTasks">

                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Иконка</th>
                                        <th>Автор</th>
                                        <th>Тема</th>
                                        <th>Статус</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php foreach ($myTasks as $key => $myTask): ?>
                                        <tr class="tr <?=Task::$statusTranslate[$myTask->status]['color']?>" data-id="<?=$myTask->id?>">
                                            <td class="taskText" style="display: none"><?=$myTask->text?></td>
                                            <td class="td color__trigger">
                                                <input type="radio" name="showDetails" title="" value="<?=$myTask->id?>"/>
                                            </td>
                                            <td class="td">
                                        <span class="glyphicon glyphicon-file">
                                            </td>
                                            <td class="td filterEmail">
                                                <form method="POST" action="/task/">
                                                    <?=$myTask->userEmail?>
                                                    <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                                                    <input type="text" value="<?=$myTask->userEmail?>" name="filterAuthor" style="display: none"/>
                                                    <input type="text" value="myTaskPanel" name="panel" style="display: none"/>
                                                </form>
                                            </td>
                                            <td class="td">
                                                <?=TaskController::generateRightTheme($myTask)?>
                                            </td>
                                            <td class="td">
                                                <?=Task::$statusTranslate[$myTask->status]['rusName']?>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                    </tbody>

                                </table>
                            <?php endif?>

                            <div class="appointedByMe">

                            </div>
                        </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
        <div class="splitter splitter-vertical-middle"></div>
        <div class="panel-right">
            <div class="panel-container-vertical">
                <div class="split splitter-horizontal splitter-left-middle">
                    <a href="#" title="Свернуть" class="panel__trigger">
                        <svg class="icon-arrow-down-mini">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow-down-mini"></use>
                        </svg>
                    </a>
                </div>
                <div class="panel-top panel-right-top"></div>

                <div class="split splitter-horizontal splitter-right-middle">
                    <a href="#" title="Свернуть" class="panel__trigger">
                        <svg class="icon-arrow-down-mini">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow-down-mini"></use>
                        </svg>
                    </a>
                </div>

                <div class="panel-middle panel-right-middle"></div>

                <div class="split splitter-horizontal splitter-right-top">
                    <a href="#" title="Свернуть" class="panel__trigger">
                        <svg class="icon-arrow-down-mini">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow-down-mini"></use>
                        </svg>
                    </a>
                </div>

                <div class="panel-bottom"></div>
            </div>
        </div>
    </div>

    <div class="split splitter-horizontal splitter-atop">
        <a href="#" title="Свернуть" class="panel__trigger">
            <svg class="icon-arrow-down-mini">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow-down-mini"></use>
            </svg>
        </a>
    </div>


    <div class="panel-abottom">
        <div class="panel-left panel-abottom-left">
            <div class="panel-container-vertical"></div>
        </div>
        <div class="splitter splitter-vertical-abottom"></div>

        <div class="panel-right panel-abottom-right">
            <div class="panel-container-vertical"></div>
        </div>
    </div>
</div>