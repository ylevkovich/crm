<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $form yii\widgets\ActiveForm */
/* @var $newTasksAssoc */
/* @var $modelUploadForm app\models\UploadForm */
?>

<div class="task-form">

    <?php $form = ActiveForm::begin([
        'options' =>[
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>

    <?php
    $params = [
        'prompt' => 'Check main task...',
    ];
    if($newTasksAssoc)
        echo $form->field($model, 'id_mainTask')->dropDownList($newTasksAssoc, $params)->label('Chose main task by theme if need');
    ?>

<!--    <div class="form-group field-task-deadline">-->
<!--        <label class="control-label" for="task-deadline">Dead line</label>-->
<!--        --><?//= DateTimePicker::widget([
//            'name' => 'Task[deadline]',
//            'id' => 'task-deadline',
//            'options' => ['placeholder' => 'Select operating time ...'],
//        ]); ?>
<!---->
<!--        <div class="help-block"></div>-->
<!--    </div>-->

    <?= $form->field($model, 'theme')->textInput() ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <!--    Upload form    -->
    <div class="panel panel-info">
        <div class="panel-heading">You can add files</div>
        <div class="panel-body">
            <?= $form->field($modelUploadForm, 'file[]')->fileInput(['multiple' => true,'class' => 'btn btn-default']) ?>
        </div>
    </div>
    <!--    Upload form end   -->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
