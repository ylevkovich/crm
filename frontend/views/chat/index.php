<?php

use yii\helpers\Html;
use frontend\controllers\UserController;

/* @var $this yii\web\View */
/* @var $users array of objects contain all users */
/* @var $modelUploadForm \frontend\models\UploadForm*/

$this->title = 'Чат';
$this->registerJsFile('/assets/f99dcc2c/jquery.js');
$this->registerJsFile('/assets/f99dcc2c/ajaxsendfile.js');
$this->registerCssFile('/css/chat/index.css');
$this->registerJsFile('/js/chat/index.js');
$this->registerJsFile('/js/notifications.js');
?>


<div class="chat-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?=Html::input('checkbox', 'showOnlyOnline')?>
    <span>Только онлайн</span>

    <div class="chat">
        <div class="users">
            <ul>
                <?php foreach($users as $user):?>
                    <li data-id="<?=$user->id?>" class="<?=UserController::is_online($user) ? 'online' : ''?>">
                        <?=Html::input('radio', 'user'/*, null, ['style' => 'display: block']*/)?>
                        <?=$user->email?>
                    </li>
                <?php endforeach;?>
            </ul>
        </div>
        <div class="messages"></div>
        <div class="inputMessage">
            <form id="sendingMsg" >
                <textarea name="text" placeholder="Введите сообщение" required>
                </textarea>
                <span id="clip" class="glyphicon glyphicon-paperclip"></span>
                <input name="idUserFrom" type="hidden" value="<?=Yii::$app->user->identity->id?>" />
                <input name="idUserTo" type="hidden"/>
            </form>

            <form action="/chat/files" method="post" enctype="multipart/form-data" id="sendFiles">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                <input id="uploadform-file" class="btn btn-default" name="UploadForm[file][]" multiple="" type="file">
            </form>
        </div>

        <div class="panel-abottom">
            <div class="panel-left panel-abottom-left">
                <div class="panel-container-vertical"></div>
            </div>
            <div class="splitter splitter-vertical-abottom"></div>

            <div class="panel-right panel-abottom-right">
                <div class="panel-container-vertical"></div>
            </div>
        </div>
    </div>
    
</div>