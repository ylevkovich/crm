<?php

namespace frontend\controllers;

use Yii;
use frontend\components\ConvertTime;
use frontend\models\Elaboration;
use common\models\User;
use yii\web\Controller;
use frontend\components\FileOperations;
use frontend\components\GoodException;
use frontend\models\History;
use frontend\models\Task;
use frontend\controllers\TaskController;
use frontend\models\File;

class HistoryController extends Controller
{
    public function actionGet_history($id_task)
    {
        if( Yii::$app->user->identity && Yii::$app->request->get() ){
            if( $id_task && $histories = History::find()
                    ->where(['id_task' => $id_task])
                    ->orderBy('id DESC')
                    ->with('user')
                    ->all() ){

                $fileContent = '';
                if($fileList = FileController::getListOfFiles($id_task)) {
                    $fileContent .= 'Вложения: ';
                    foreach (FileController::getListOfFiles($id_task) as $filename) {
                        $fileContent .= ' '.$filename;
                    }
                }

                $forJsonHistory = [];
                foreach($histories as $key => $history){
                    $forJsonHistory[$key] = [
                        'action'        => $history->action,
                        'created'       => ConvertTime::toDatetime($history->created),
                        'userEmail'     => $history->user->email,
                        'taskUserEmail' => $history->task->user->email,
                        'theme'         => $history->task->theme,
                        'text'          => $history->task->text,
                        'attached'      => $history->action == History::$actions['create'] ? $fileContent : null
                    ];
                }

                echo json_encode($forJsonHistory);
            }else echo json_encode(['error' => 'Input valid id task...']);
        }else echo json_encode(['error' => 'Error: Log in, Please or generate post query...']);
    }
    
    /**
     * Adds action to history
     * @param Task $task
     * @param $action
     * @return bool
     * @throws GoodException
     */
    public static function addToHistory( Task $task, $action)
    {
        if( self::fillAndSaveData($task, $action) )
        {
            FileOperations::openOrCreateAndWrite(TaskController::getTaskDir($task).'/'.'HISTORY.txt', self::generateFileContent($task, $action));
            return true;
        }else{
            return false;
        }
    }

    /**
     * Fills and save data record by id task
     * @param Task $task
     * @param $action
     * @return bool
     * @throws GoodException
     */
    private static function fillAndSaveData(Task $task, $action){
        $history = new History();
        $history->id_task = $task->id;
        $history->action = $action;
        $history->created = date('U');
        if( $action == History::$actions['create'] || $action == History::$actions['elaborationAnswer'] ){
            $history->id_user = $task->user->id;
        }else{
            $history->id_user = Yii::$app->user->identity->id;
        }

        if( !$history->save() )
            throw new GoodException('Error', 'Error saving History data...');
        return true;
    }

    /**
     * Returns string of content for writing to history file
     * @param Task $task
     * @param integer $action
     * @return string
     * @throws GoodException
     */
    private static function generateFileContent(Task $task, $action){
        switch($action){
            case History::$actions['create']:{
                $fileContent = ConvertTime::toDatetime(date('U')).' - Автор - '.$task->user->email.PHP_EOL
                    .'Тема: '.TaskController::generateRightTheme($task).PHP_EOL
                    .'Текст: '.$task->text.PHP_EOL;

                if($fileList = FileController::getListOfFiles($task->id)){
                    $fileContent .= 'Вложения: ';
                    foreach (FileController::getListOfFiles($task->id) as $filename){
                        $fileContent .= $filename.PHP_EOL;
                    }
                }
                break;
            }
            case History::$actions['appoint']:{
                $fileContent = ConvertTime::toDatetime(date('U')).PHP_EOL
                    .Yii::$app->user->identity->email.' '
                    .History::$actionsTranslate[$action].': '.User::findOne(['id' => $task->appointed])['email'];
                break;
            }
            case History::$actions['abandon']:{
                $fileContent = ConvertTime::toDatetime(date('U')).PHP_EOL
                    .Yii::$app->user->identity->email.' '
                    .History::$actionsTranslate[$action];
                break;
            }
            case History::$actions['elaborationQuestion']:{
                $fileContent = ConvertTime::toDatetime(date('U')).PHP_EOL
                    .Yii::$app->user->identity->email.' '
                    .History::$actionsTranslate[$action];
                break;
            }
            case History::$actions['elaborationAnswer']:{
                $fileContent = ConvertTime::toDatetime(date('U')).PHP_EOL
                    .$task->user->email.' '
                    .History::$actionsTranslate[$action];
                break;
            }
            case History::$actions['closingTask']:{
                $fileContent = ConvertTime::toDatetime(date('U')).PHP_EOL
                    .Yii::$app->user->identity->email.' '
                    .History::$actionsTranslate[$action];
                break;
            }
        }

        return $fileContent;
    }
}
