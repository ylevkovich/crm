<?php

namespace frontend\controllers;

use frontend\models\Settings;
use Yii;
use frontend\models\History;
use frontend\models\User;
use frontend\models\Task;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\console\Exception;
use frontend\components\GoodException;
use frontend\components\ConvertTime;
use frontend\models\UploadForm;
use frontend\controllers\FileController;
use frontend\models\File;
use frontend\components\Email;
use frontend\components\PostClient;
use frontend\models\SignupForm;
use frontend\components\FileOperations;
use frontend\models\Elaboration;
use frontend\controllers\ElaborationController;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends Controller
{
    private $userEmail;
    public static $dirOfCurrentTask = NULL;

    /**
     * Lists all Task models.
     * @return string
     * @throws GoodException
     */
    public function actionIndex()
    {
        if( !Yii::$app->user->identity )
            return $this->redirect('/site/login');

        if( $settings = Settings::find()->one() ){
            $uploadDir = $settings->upload_dir;
        }else throw new GoodException('Error', 'Some problem with Settings table..');

        $this->layout = 'tasks';
        $filters = false;
        $modelUploadForm = new UploadForm();

        //Filters
        if(Yii::$app->request->post()){
            if( Yii::$app->request->post('panel') && Yii::$app->request->post('panel') == 'myTaskPanel'){
                $todayTasks =  Task::find()->joinWith('user')->joinWith('files')->where([
                    'task.appointed'=> null,
                    'task.status'=> [Task::$status['new'], Task::$status['overdue']]
                ])->all();

                $data = $this->filterTasks();
                $myTasks = $data['tasks'];
                $filters = $data['filters'];
            }else{
                $myTasks = Task::find()->joinWith('user')->where([
                    'task.status'=> Task::$status['appointed'],
                    'task.appointed'=> Yii::$app->user->identity->id
                ])->all();

                $data = $this->filterTasks();
                $todayTasks = $data['tasks'];
                $filters = $data['filters'];
            }
        }else{
            $todayTasks =  Task::find()->joinWith('user')->joinWith('files')->where([
                'task.appointed'=> null,
                'task.status'=> [Task::$status['new'], Task::$status['overdue']]
            ])->all();

            $myTasks = Task::find()->joinWith('user')->where([
                'task.status'=> [Task::$status['appointed'],Task::$status['inElaboration']],
                'task.appointed'=> Yii::$app->user->identity->id
            ])->all();
        }

        return $this->render('index', [
            'todayTasks' => $todayTasks,
            'filters' => $filters,
            'filterPanel' => Yii::$app->request->post('panel') ? 'myTasks' : null,
            'myTasks' => $myTasks,
            'managers' => User::find()->all(),
            'modelUploadForm' => $modelUploadForm,
            'uploadDir' => $uploadDir,
        ]);
    }

    /**
     * Echos JSON-encoded list of tasks appointed given user 
     * @param $id_user
     */
    public function actionGet_user_tasks($id_user)
    {
        if( Yii::$app->user->identity->position == User::$pos['admin'] ){
            if( $tasks = Task::find()->where(['appointed' => $id_user])->with('user')->all()){
                $jsonTasks = [];
                foreach($tasks as $key => $task){
                    $jsonTasks[$key] = [
                        'author' => $task->user->email,
                        'theme'  => self::generateRightTheme($task),
                        'status' => Task::$statusTranslate[$task->status]['rusName']
                    ];
                }

                echo json_encode($jsonTasks);
            }else{
                echo json_encode(['Error' => 'No any tasks']);
            }
        }else{
            $this->redirect('/site/login');
        }
    }

    /**
     * Creates a new Task model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     * @throws GoodException
     */
    public function actionCreate()
    {
        $model = new Task();
        $modelUploadForm = new UploadForm();
        $modelFiles = new File();

        if ($model->load(Yii::$app->request->post())) {
            try{
                //created
                $model->created = date('U');

                if( isset(Yii::$app->request->post()['Task']['email']) ){
                    //email
                    $model->email = Yii::$app->request->post()['Task']['email'];
                    $model->id_user = NULL;
                }else{
                    //id_user
                    $model->id_user = Yii::$app->user->id;
                    $model->email = NULL;
                }
                if( !$model->email && !$model->id_user )
                    throw new GoodException('Error', 'Please login or input email...');

                //status
                $model->status = 0;

                //deadline
                Yii::$app->request->post()['Task']['deadline'] ? $model->deadline = ConvertTime::toU(Yii::$app->request->post()['Task']['deadline']) : NULL;
                if( $model->deadline && $model->deadline <= $model->created)
                    throw new GoodException('Error', 'Input right dead line...');

                //id_mainTask
                isset(Yii::$app->request->post()['Task']['id_mainTask']) ? $model->id_mainTask = Yii::$app->request->post()['Task']['id_mainTask']  : NULL;

                if( !$model->save() )
                    throw new GoodException('Error', 'Saving data...');

                //files
                FileController::uploadTasksFiles($model->id);

                if($model->email) {
                    $to = $model->email;
                }elseif($model->id_user && $user = User::findOne(['id' => $model->id_user])){
                    if(!$user->email)
                        throw new GoodException('Error','In user data no email info...');
                    $to = $user->email;
                }else{
                    throw new GoodException('Error','No email address...');
                }
                
                Email::send("\n 
                    Ваша заявка №$model->id : \n
                    Tема: $model->theme \n
                    Зарегистрирована: ".ConvertTime::toDatetime($model->created),
                    $to
                    );
                    
                return $this->redirect(['/task']);
            }catch(Exception $e){
                return $e->getMessage();
            }
        } else {
            //$newTasks for drop down of main tasks
            $newTasks = Task::find()->where(['status' => 1])->all();
            foreach($newTasks as $key => $newTask)
                $newTasksAssoc[$newTask->id] = $newTask->id.': '.$newTask->theme;

            return $this->render('create', [
                'model' => $model,
                'modelUploadForm' => $modelUploadForm,
                'newTasksAssoc' => isset($newTasksAssoc) ? $newTasksAssoc : NULL,
            ]);
        }
    }

    /**
     * Gets all messages from mailbox, parses them, save info
     * to db, creates for each directory and saves attached files
     * there, deletes read messages, creates "BEGIN.txt" file
     * @throws GoodException
     */
    public function actionGet_inbox_mails()
    {
        if( $settings = Settings::find()->one() ){
            $postClient = new PostClient('imap.gmail.com', $settings->inputPost_port,'/imap/ssl',$settings->inputPost_email, $settings->inputPost_password);

            if($messages = $postClient->getMessages())
                foreach($messages as $key => $message){
                    if( preg_match('/^Re: \[#/', $message['subject']) ){
                        ElaborationController::elaborationAnswerProcedure($message);
                        $postClient->deleteMessageFromMailbox($key + 1);
                    }else{
                        if( $task = $this->createTaskFromMailbox($message) ){
                            if( $this->createTasksDirectory($task) ){
                                if( isset($message['files']) )
                                    $this->getAttachedFilesFromMailbox($message, $task->id);
                                $postClient->deleteMessageFromMailbox($key + 1);
                                $this->createTaskBeginFile($task);
                                HistoryController::addToHistory($task, History::$actions['create']);
                            }
                        }
                    }
                }

            $this->redirect('/task');

        }else throw new GoodException('Error', 'Some problem with Settings table..');
    }

    /**
     * For ajax query.
     * Writes in field inReadingNow from table Task users id,
     * who has clicked on task in work panel
     * @param $id_task. Chosen task
     */
    public function actionAdd_reader_to_the_task($id_task){
        if( Yii::$app->request->isPost && isset(Yii::$app->user->identity) ){
            if( $id_task = intval($id_task) ){
                if( $task = Task::findOne(['id' => $id_task]) ){
                    if( NULL == $task->isReadingNow )
                        $task->isReadingNow = Yii::$app->user->identity->id;
                    $task->lastReadTime = date('U');
                    if( $task->save() ) echo 'Success';
                    else echo 'Error saving data';
                }else echo 'No task with this id';
            }else echo 'Wrong handed task id';
        }else echo 'Not authenticated';
    }

    /**
     * For ajax query.
     * Clear field isReadingNow in table Task where id = $id_task
     * @param $id_task
     */
    public function actionDelete_reader_from_the_task($id_task){
        if( Yii::$app->request->isPost && isset(Yii::$app->user->identity) ){
            if( $id_task = intval($id_task) ){
                if( $task = Task::findOne(['id' => $id_task]) ){
//                    if( $task->isReadingNow == Yii::$app->user->identity->id )
                    $task->isReadingNow = NULL;
                    if( $task->save() ) echo 'Success';
                    else echo 'Error saving data';
                }else echo 'No task with this id';
            }else echo 'Wrong handed task id';
        }else echo 'Not authenticated';
    }

    /**
     * For ajax query.
     * Clear fields isReadingNow in table Task where id = current user
     */
    public function actionDelete_reader_from_tasks(){
        if( Yii::$app->request->isPost && isset(Yii::$app->user->identity) ){
                if( $tasks = Task::findAll(['isReadingNow' => Yii::$app->user->identity->id]) ){
                    foreach( $tasks as $task ){
                        $task->isReadingNow = NULL;
                        if( $task->save() ) echo 'Success';
                        else echo 'Error saving data';
                    }
                }else echo 'No task with this id';
        }else echo 'Not authenticated';
    }

    /**
     * For ajax query
     * Getting list of users(emails and last reading time)
     * and tasks, what each are reading
     * @return null|array
     */
    public function actionGet_readers_of_tasks(){

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = Task::find()->joinWith('user')->all();

        foreach ($model as $task){
            $arr[$task->id] = [
                'userReadingEmail' => $task->UserReadingEmail,
                'lastReadTime' => $task->lastReadTime,
                'status' => $task->status
            ];
        }

        return isset($arr) ? $arr : NULL;
    }

    /**
     * Changes statuses after specified time
     */
    public function actionCheck_tasks_statuses($overdueAll = false)
    {
        if( $settings = Settings::find()->one() ){
            $overdueTime = $settings->reactionTime;
            
            if($tasks = Task::find()->all()){
                foreach($tasks as $task){
                    switch ($overdueAll){
                        case true:{
                            $task->status = Task::$status['overdue'];
                            if( !$task->save() )
                                echo "Error saving data with id_task = $task->id";
                            break;
                        }
                        case false:{
                            if( $task->created && $task->created + $overdueTime < date('U') ){
                                $task->status = Task::$status['overdue'];
                                if( !$task->save() )
                                    echo "Error saving data with id_task = $task->id";
                            }
                            break;
                        }
                    }
                }
            }
        }else throw new GoodException('Error', 'Some problem with Settings table..');
    }

//    /**
//     * Returns JSON encoded array of files linked to the task
//     * @param $id_task
//     */
//    public function actionGet_task_files($id_task)
//    {
//        $arrFiles = [];
//        if( $files = File::findAll(['id_task' => $id_task]) ){
//            foreach( $files as $file ){
//                $arrFiles['name'] = $file->name;
//            }
//            echo json_encode($arrFiles);
//        }else{
//            echo null;
//        }
//    }

    /**
     * Appoints task to chosen user, if id_user == null,
     * that's means what user has took task himself
     * @param $id_task
     * @param null $id_user
     */
    public function actionAppoint_task($id_task, $id_user = null)
    {
        if( Yii::$app->request->isPost && isset(Yii::$app->user->identity) ) {
            if( $task = Task::findOne(['id' => $id_task]) ){
                $task->appointed = !is_null($id_user) ? $id_user : Yii::$app->user->identity->id;
                $task->status = Task::$status['appointed'];
                if( $task->save() )
                    echo json_encode(['success' => 'Successfully saved...']);

                HistoryController::addToHistory($task, History::$actions['appoint']);
            } else echo json_encode(['error' => 'Error: no such id task...']);
        }else echo json_encode(['error' => 'Error: Log in, Please ...']);
    }

    /**
     * Abandoning task
     * @param $id_task
     * @throws GoodException
     */
    public function actionAbandon_from_task($id_task)
    {
        if( Yii::$app->user->identity ){
            if( $task = Task::findOne(['id' => $id_task]) ){
                if( $task->appointed == Yii::$app->user->id ){
                    $position = User::findOne(['id' => Yii::$app->user->id])['position'];
                    if( $position == User::$pos['manager'] || $position == User::$pos['customer'] || $position == User::$pos['admin'] ) {
                        $task->status = Task::$status['new'];
                        $task->lastReadTime = null;
                        $task->appointed = null;
                        if( !$task->save() )
                            throw new GoodException('Error', 'Error in assigning task new status...');

                        HistoryController::addToHistory($task, History::$actions['abandon']);
                        
                        $this->redirect('/task/');
                    }else{
                        throw new GoodException('Error', 'In development...');
                    }
                }else throw new GoodException('Error', 'This task not belong to you...');
            }else throw new GoodException('Error', 'No such task...');
        }else $this->redirect('/site/login');
    }

    /**
     * Returns json array of some task info
     * @param $id_task
     */
    public function actionGet_task_in_json($id_task)
    {
        if( Yii::$app->user->identity ){
            if( $id_task &&
                $task = Task::find()
                    ->where(['id' => $id_task])
                    ->with('user')
                    ->one() ){

                echo json_encode([
                    'theme'     => $this->generateRightTheme($task),
                    'text'      => $task->text,
                    'author'    => $task->user->email,
                    'id_author' => $task->user->id
                ]);

            }else echo json_encode(['error' => 'Input valid id task...']);
        }else echo json_encode(['error' => 'Error: Log in, Please...']);
    }

    /**
     * - Sends finished letter;
     * - Registers in Elaboration finish letter
     * - Changes task status to closed
     * - Creates END.txt file
     * - Redirects to /task/
     * @return \yii\web\Response
     * @throws GoodException
     */
    public function actionClose_task(){
        if( Yii::$app->user->identity ){
            if( Yii::$app->request->post('recipient') && Yii::$app->request->post('theme') && Yii::$app->request->post('taskId') ){
                if(Email::sendHtml(
                    Yii::$app->request->post('fusdfll'),
                    Yii::$app->request->post('recipient'),
                    Yii::$app->request->post('theme')) )
                {
                    if( ElaborationController::fillDataClosingTask() ){
                        $task = $this->changeTaskStatus(Yii::$app->request->post('taskId'), Task::$status['closed']);
                        HistoryController::addToHistory($task, History::$actions['closingTask']);
                        FileOperations::openOrCreateAndWrite($this->getTaskDir($task).'/'.'END.txt', strip_tags(Yii::$app->request->post('fusdfll')));

                        return $this->redirect('/task');
                    }
                }
            }else throw new GoodException('Error', 'No enough data for send...');
        }else $this->redirect('/site/login');
    }

    /**
     * Changes task status
     * @param $id_task
     * @param $status
     * @return null|static
     * @throws GoodException
     */
    private function changeTaskStatus($id_task, $status){
        if( $task = Task::findOne(['id' => intval($id_task)]) ){
            $task->status = $status;
            if( !$task->save() )
                throw new GoodException('Error', 'Error change task status...');
            return $task;
        }else throw new GoodException('Error', 'Can\'t find task with specified id...');
    }

    /**
     * Returns object to specified format for showing
     * @param Task $theme
     * @return string
     * @throws GoodException
     */
    public static function generateRightTheme(Task $theme)
    {
        return '[#'.ConvertTime::toDatetime_YmdGis($theme->created).'_'.$theme->id.'] '.$theme->theme;
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates record in table task by given array
     * @param $mail
     * @return Task
     * @throws GoodException Error saving data
     */
    private function createTaskFromMailbox($mail)
    {
        $task = new Task();
        $task->created = $mail['date'];

        if( is_null($mail['from']['mailbox']) || is_null($mail['from']['host']) )
            throw new GoodException('Error', 'Can\'t take email info...');
        $email = $mail['from']['mailbox'].'@'.$mail['from']['host'];

        if( !$user = $this->checkExistingUser($email))
            $user = $this->addUserByEmail($email);

        $task->id_user = $user->id;
        $task->status = Task::$status['new'];
        $task->theme = $this->cutTheme($mail['subject']);
        $task->text = $mail['body'];

        if( !$task->save() )
            throw new GoodException('Error', 'Error saving data...');

        return $task;
    }

    /**
     * Returns user with this email, if such exists
     * @param $email
     * @return array|null|\yii\db\ActiveRecord
     */
    private function checkExistingUser($email)
    {
        return User::find()->where(['email' => $email])->one();
    }

    /**
     * Creates user with having email and generates password
     * @param $email.
     * @return \common\models\User|null
     */
    private function addUserByEmail($email){
        $singUpForm = new SignupForm();
        $singUpForm->email = $email;
        $singUpForm->password = Yii::$app->security->generateRandomString(4);
        return $singUpForm->signup();
    }

    /**
     * Cuts theme for writing to database
     * @param $theme
     * @return string
     */
    private function cutTheme($theme)
    {
        if( strlen($theme) > 10){
            $theme = iconv('UTF-8','windows-1251',$theme );
            $theme = substr($theme, 0, 7).'...';

            return iconv('windows-1251','UTF-8',$theme );
        }else{
            return $theme;
        }
    }

    /**
     * Creates directory Upload, if not exist,
     * there creates directory what has name respectively users email + _ + datetime,
     * there creates directory what has name datetime + _ + task id
     * @param $task. Created object of task
     * @return bool
     * @throws GoodException
     */
    private function createTasksDirectory($task){
        if( $settings = Settings::find()->one() ){
            $uploadDir = $settings->upload_dir;

            $this->userEmail = User::find()->where(['id' => $task->id_user])->one()->email;
            $datetime = ConvertTime::toDatetime_YmdGis($task->created);

            FileController::createDir($uploadDir, $this->userEmail);
            FileController::createDir($uploadDir.'/'.$this->userEmail, $this->userEmail.'_'.$datetime);

            return self::$dirOfCurrentTask = FileController::createDir($uploadDir.'/'.$this->userEmail.'/'.$this->userEmail.'_'.$datetime, $datetime.'_'.$task->id);
            
        }else throw new GoodException('Error', 'Some problem with Settings table..');
    }

    /**
     * Creates begin files due to requirement
     * @param $task
     */
    private function createTaskBeginFile($task){
        //Todo input next data from some tools file
        if( $pathToTasksDir = $this->createTasksDirectory($task) )
            FileOperations::openOrCreateAndWrite($pathToTasksDir.'/'.'BEGIN.txt',$this->formationBeginFile($task));
    }

    /**
     * Returns format of text for writing in the file
     * @param $task
     * @return string
     */
    private function formationBeginFile($task){
        return "
        От кого: $this->userEmail
        Тема: $task->theme
        $task->text
        ";
    }

    /**
     * Saves attached files to the directory of task
     * and write each file to db
     * @param $message
     * @param $id_task
     * @throws GoodException
     */
    public static function getAttachedFilesFromMailbox($message, $id_task){
        if( !is_null(self::$dirOfCurrentTask) )
            foreach($message['files'] as $key => $file){
                FileOperations::openOrCreateAndWrite(self::$dirOfCurrentTask.'/'.iconv('utf-8', 'WINDOWS-1251', $file['filename']), $file['content']);
                $modelFile = new File();
                $modelFile->id_task = $id_task;
                $modelFile->name = $file['filename'];
                if( !$modelFile->save())
                    throw new GoodException('Error', 'Can\'t save data file '.$file['filename'].'...');
            }
    }

    /**
     * Filters data
     * @return array filtered tasks and filter name
     * @throws GoodException
     */
    private function filterTasks(){
        //By time
        if( Yii::$app->request->post('dateFrom') ){
            $dateFrom = Yii::$app->request->post('dateFrom');
            $dateTo = Yii::$app->request->post('dateTo');
            $tasks = Task::find()
                ->where('created > '.ConvertTime::toU($dateFrom))
                ->andWhere('created < '.ConvertTime::toU($dateTo))
                ->all();

            $filters[] =  $dateFrom.' - '.$dateTo;
            //By author
        }else if( Yii::$app->request->post('filterAuthor') ){
            if( Yii::$app->request->post('panel') && Yii::$app->request->post('panel') == 'myTaskPanel'){
                $tasks = Task::find()
                    ->joinWith('user')
                    ->where([
                        'email' => Yii::$app->request->post('filterAuthor'),
                        'task.appointed'=> Yii::$app->user->id
                    ])
                    ->all();
            }else{
                $tasks = Task::find()
                    ->joinWith('user')
                    ->where([
                        'email' => Yii::$app->request->post('filterAuthor'),
                        'task.appointed'=> null,
                        'task.status'=> [Task::$status['new'], Task::$status['overdue']]
                    ])
                    ->all();
            }
            $filters[] =  Yii::$app->request->post('filterAuthor');
            //By status
        }else if( Yii::$app->request->post('filterStatus') || Yii::$app->request->post('filterStatus') == 0 ){
            $tasks = Task::find()
                ->where(['status' => Yii::$app->request->post('filterStatus')])
                ->all();

            $filters[] =  Yii::$app->request->post('filterStatusName') ? Yii::$app->request->post('filterStatusName') : 'Status: '.Yii::$app->request->post('filterStatus');
        }else{
            throw new GoodException('Error', 'Error getting date for filter...');
        }

        return [
            'tasks' => $tasks,
            'filters' => $filters
        ];
    }

    /**
     * Returns path to the task directory
     * @param $task
     * @return string
     * @throws GoodException
     */
    public static function getTaskDir($task){
        if( $settings = Settings::find()->one() ){
            $uploadDir = $settings->upload_dir;

            return $uploadDir.'/'
            .$task->user->email.'/'
            .$task->user->email.'_'.ConvertTime::toDatetime_YmdGis($task->created).'/'
            .ConvertTime::toDatetime_YmdGis($task->created).'_'.$task->id;

        }else throw new GoodException('Error', 'Some problem with Settings table..');
    }
}
