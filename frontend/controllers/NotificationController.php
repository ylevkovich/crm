<?php

namespace frontend\controllers;

use frontend\components\ConvertTime;
use frontend\components\GoodException;
use frontend\models\Chat;
use frontend\models\Notification;
use frontend\models\Task;
use Yii;
use yii\web\Controller;

class NotificationController extends Controller
{
    /**
     * Add Notification record
     * @param $id
     * @param $type
     * @return bool
     * @throws GoodException
     */
    public static function addNotification($id, $type, $id_user_to){
        $notification = new Notification();

        if(array_key_exists($type, Notification::$type))
            $notification->type = Notification::$type[$type];
        else throw new GoodException('Error', 'This key is not belong to type array..');

        $notification->id_elaboration_or_chat = $id;
        $notification->id_user_to = $id_user_to;
        return $notification->save();
    }

    /**
     * Gets end echos JSON-encoded chat & elaboration notifications,
     * who belongs to current user
     * @throws GoodException
     */
    public function actionGet_notifications()
    {
        if(Yii::$app->user->identity){
            if($notifications = Notification::findAll(['id_user_to' => Yii::$app->user->id])){
                $jsonNotifications = [];
                foreach($notifications as $key => $notification){
                    if( $notification->type == Notification::$type['elaboration'] ){
                        if( $task = Task::find()->where(['id' => $notification->id_elaboration_or_chat])->with('user')->one()){
                            $jsonNotifications[$key] = [
                                'id_notification' => $notification->id,
                                'type' => $notification->type,
                                'theme' => TaskController::generateRightTheme($task),
                                'author' => $task->user->email
                            ];
                        }else $jsonNotifications = 'No such task id';
                    }elseif($notification->type == Notification::$type['chat']){
                        if( $chat = Chat::find()->where(['id' => $notification->id_elaboration_or_chat])->with('idUserFrom')->one() ){
                            $jsonNotifications[$key] = [
                                'id_notification' => $notification->id,
                                'type' => $notification->type,
                                'email' => $chat->idUserFrom->email,
                                'datetime' => ConvertTime::toDatetime($chat->created_at)
                            ];
                        }else $jsonNotifications = 'No such chat id';
                    }else{
                        $jsonNotifications = 'Error with notification type...';
                    }
                }
            }

            echo isset($jsonNotifications) ? json_encode($jsonNotifications) : json_encode('');
        }
    }

    /**
     * Deletes notification
     * @param $id
     * @return bool|int
     */
    public function actionDelete($id)
    {
        if( Yii::$app->user->identity ){
            return Notification::deleteAll(['id' => $id]);
        }else{
            return false;
        }
    }
}
