<?php

namespace frontend\controllers;

use Yii;
use frontend\components\ConvertTime;
use frontend\models\History;
use frontend\components\Email;
use frontend\components\GoodException;
use frontend\models\Elaboration;
use frontend\models\Task;
use frontend\components\FileOperations;
use frontend\controllers\NotificationController;

class ElaborationController extends \yii\web\Controller
{
    /**
     * @var Task;
     */
    private static $currentTask;

    /**
     * Processes elaboration question:
     * - saving data to db,
     * - changes status in task record to in elaboration
     * - create "Correction.txt" file
     * @throws GoodException
     */
    public function actionIndex()
    {
        if( Yii::$app->user->identity && Yii::$app->request->post() ){
            if( Email::send(
                Yii::$app->request->post('text'),
                Yii::$app->request->post('recipient'),
                Yii::$app->request->post('theme')
            ) ){
                self::$currentTask = Task::find()->where(['id' => Yii::$app->request->post('taskId')])->with('user')->one();
                self::$currentTask->status = Task::$status['inElaboration'];
                if( !self::$currentTask->save() )
                    throw new GoodException('Error', 'Can\'t change status of task');

                if( $elaboration = self::fillElaborationQuestionData() ){
                    self::createCorrectionFile($elaboration);
                    HistoryController::addToHistory(self::$currentTask, History::$actions['elaborationQuestion']);
                    $this->redirect('/task/');
                }
            }else{
                throw new GoodException('Error', 'Your message has not sent...');
            }
        }else $this->redirect('/site/login');
    }

    /**
     * Returns all Elaboration records with specified id_task in json format
     * @param $id_task
     * @throws GoodException
     */
    public function actionGet_elaboration_dialog_in_json($id_task)
    {
        if( Yii::$app->user->identity ){
            if( $id_task && $elaborations = Elaboration::find()
                    ->where(['id_task' => $id_task])
                    ->with('user')
                    ->orderBy('created DESC')
                    ->all() )
            {

                $arr = [];
                foreach($elaborations as $key => $elaboration){
                    $arr[$key]['type'] = Elaboration::$typeTranslate[$elaboration->type];
                    $arr[$key]['from'] = $elaboration->user->email;
                    $arr[$key]['created'] = ConvertTime::toDatetime($elaboration->created);
                    $arr[$key]['text'] = $elaboration->text;
                }

                echo json_encode($arr);
            }else echo json_encode(['error' => 'Input valid id task...']);
        }else echo json_encode(['error' => 'Error: Log in, Please...']);
    }

    /**
     * Gets task id from elaboration message,
     * gets Task record by specified id,
     * changes status on appointed,
     * fills Elaboration record and save,
     * saves all attached files to task directory
     * @param $message
     * @throws GoodException
     */
    public static function elaborationAnswerProcedure($message){
        $id_task = self::getIdTaskFromElaborationAnswer($message['subject']);
        if( $task = Task::find()->where(['id' => $id_task])->with('user')->one() ){
            $task->status = Task::$status['appointed'];
            if( !$task->save() )
                throw new GoodException('Error', 'Error changing task status...');
            self::$currentTask = $task;
            if( $elaboration = self::fillElaborationAnswerData($message) ){
                self::saveAttachedFiles($message);
                self::createCorrectionFile($elaboration);
                HistoryController::addToHistory(self::$currentTask, History::$actions['elaborationAnswer']);
                NotificationController::addNotification($task->id, 'elaboration', $task->appointed);
            }else{
                throw new GoodException('Error', 'Error in creating Elaboration object...');
            }
        }else{
            throw new GoodException('Error', 'No such task specified id task');
        }
    }

    /**
     * Records data to Elaboration
     * @param $message
     * @return Elaboration
     * @throws GoodException
     */
    public static function fillElaborationAnswerData($message){
        $elaboration = new Elaboration();

        if( self::$currentTask ) $elaboration->id_task = self::$currentTask->id;
        else throw new GoodException('Error', 'Didn\'t created Task object...');

        $elaboration->type = Elaboration::$type['answer'];
        $elaboration->id_author = self::$currentTask->id_user;
        $elaboration->created = $message['date'];
        $elaboration->text = self::getElaborationMessageBody($message['body']);

        if( !$elaboration->save() )
            throw new GoodException('Error', 'Error save elaboration data...');
        else
            return $elaboration;
    }


    /**
     * Saves elaboration question
     * @return Elaboration|bool
     * @throws GoodException
     */
    private static function fillElaborationQuestionData(){
        if( Yii::$app->user && Yii::$app->request->post() ){
            $elaboration = new Elaboration();

            //id_task
            if( !is_null(Yii::$app->request->post('taskId')) ){
                $elaboration->id_task = Yii::$app->request->post('taskId');
            }else{
                throw new GoodException('Error', 'Task id have to assigned in post query...');
            }

            //type
            $elaboration->type = Elaboration::$type['question'];

            //id_author
            $elaboration->id_author = Yii::$app->user->id;

            //created
            $elaboration->created = date('U');

            //text
            if( !is_null(Yii::$app->request->post('text')) )
                $elaboration->text = Yii::$app->request->post('text');
            else
                throw new GoodException('Error', 'Text of task id have to assigned in post query...');

            if( !$elaboration->save() )
                throw new GoodException('Error', 'Can\'t save elaboration data');

            return $elaboration;
        }else{
            return false;
        }
    }

    /**
     * Returns id task specified elaboration theme
     * @param $theme string
     * @return mixed
     * @throws GoodException
     */
    private static function getIdTaskFromElaborationAnswer($theme)
    {
        preg_match("/Re: \[#([0-9]){12}_([^\/]+)]/", $theme, $matches);
        if( isset($matches[2]) ){
            return $matches[2];
        }else{
            throw new GoodException('Error', 'Error specified elaboration theme...');
        }
    }

    /**
     * Saves attached files to elaboration message if that isset
     */
    private static function saveAttachedFiles($message){
        if( isset($message['files']) ){
            if( self::$currentTask ){
                TaskController::$dirOfCurrentTask = TaskController::getTaskDir(self::$currentTask);
                TaskController::getAttachedFilesFromMailbox($message, self::$currentTask->id);
            }else throw new GoodException('Error', 'Didn\'t created Task object...');
        }
    }

    /**
     * Returns has parsed body message
     * @param $body string elaboration message body
     * @return mixed
     * @throws GoodException
     */
    private static function getElaborationMessageBody($body){
        if(preg_match("/   >/", $body)){
            $matches = preg_split("/   >/", $body);
        }elseif(preg_match("/\n\n\n>/", $body)){
            $matches = preg_split("/\n\n\n>/", $body);
        }elseif(preg_match("/\n\n/", $body)){
            $matches = preg_split("/\n\n/", $body);
        }else{
            throw new GoodException('Error', 'Error in parsing answer to elaboration message...');
        }

        return $matches[0];
    }

    /**
     * Creates 'Correction.txt' file
     * @param $elaboration Elaboration
     * @throws GoodException
     */
    private static function createCorrectionFile($elaboration){
        if( self::$currentTask ){
            $str =
                Elaboration::$typeTranslate[$elaboration->type].':'         .PHP_EOL
                .'От кого:'.self::$currentTask->user->email                 .PHP_EOL
                .ConvertTime::toDatetime($elaboration->created)             .PHP_EOL
                .'Текст '.Elaboration::$typeTranslate[$elaboration->type].'а:'
                .$elaboration->text                                         .PHP_EOL;

            FileOperations::openOrCreateAndWrite(TaskController::getTaskDir(self::$currentTask).'/'.'Correction.txt', $str);
        }else throw new GoodException('Error', 'Didn\'t created Task object...');
    }

    /**
     * Create Elaboration record with closing status
     * @return Elaboration
     * @throws GoodException
     */
    public static function fillDataClosingTask(){
        $elaboration = new Elaboration();
        $elaboration->id_task = Yii::$app->request->post('taskId');
        $elaboration->type = Elaboration::$type['closing'];
        $elaboration->id_author = Yii::$app->user->id;
        $elaboration->created = date('U');
        $elaboration->text = strip_tags(Yii::$app->request->post('fusdfll'));

        if( !$elaboration->save() )
            throw new GoodException('Error', 'Error saving elaboration data...');
        return $elaboration;
    }
}
