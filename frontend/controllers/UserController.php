<?php

namespace frontend\controllers;

use Yii;
use frontend\models\User;
use frontend\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\components\GoodException;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $managersDataProvider = $searchModel->search(Yii::$app->request->queryParams, ['position' => User::$pos['manager']]);
        $customersDataProvider = $searchModel->search(Yii::$app->request->queryParams, ['position' => User::$pos['customer']]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'managersDataProvider' => $managersDataProvider,
            'customersDataProvider' => $customersDataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Adds user last activity
     * @throws GoodException
     */
    public function actionAdd_activity()
    {
        if( Yii::$app->user->identity ){
            if( $user = User::findOne(['id' => Yii::$app->user->id])){
                $user->last_activity = date('U');
                if( !$user->save() )
                    throw new GoodException('Error', 'Error changing user last_activity...');
            }else{
                throw new GoodException('Error', 'Error find user data...');
            }
        }else{
            $this->redirect('/');
        }
    }

    /**
     * If user was in activity last 15m returns true
     * @param user $user
     * @return bool
     */
    public static function is_online(User $user)
    {
        if( !is_null($user->last_activity) ){
            if( date('U') - $user->last_activity < 15*60 )
                return true;
        }

        return false;
    }

    /**
     * Echos users activities in JSON-format
     */
    public function actionGet_users_activities()
    {
        if( Yii::$app->user->identity ){
            if( $users = User::find()->all() ){
                $arr = [];
                foreach($users as $key => $user){
                    $arr[$user->id]['is_online'] = self::is_online($user) ? 1 : 0;
                }

                echo json_encode($arr);
            }else echo json_encode(['error' => 'No users...']);
        }else{
            $this->redirect('/');
        }
    }
}
