<?php

namespace frontend\controllers;

use frontend\components\ConvertTime;
use frontend\components\FileOperations;
use frontend\models\UploadForm;
use frontend\models\User;
use Yii;
use frontend\models\Chat;
use yii\web\Controller;
use yii\filters\VerbFilter;
use frontend\controllers\NotificationController;

/**
 * ChatController implements the CRUD actions for chat model.
 */
class ChatController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Shows chat panel
     * @return mixed
     */
    public function actionIndex()
    {
        if( Yii::$app->user->identity ){
            $users = User::find()->all();

            return $this->render('index', [
                'users' => $users,
                'chat' => new Chat(),
                'modelUploadForm' => new UploadForm(),
            ]);
        }else{
            $this->redirect('/site/login');
        }
    }

    /**
     * Saves Chat record to db
     * @param $id_user_to
     * @param $text
     */
    public function actionSend_message($id_user_to, $text)
    {
        if( Yii::$app->user->identity ){
            if( !is_null($id_user_to) && !is_null($text)){
                if( $chat = $this->filldata($id_user_to, $text)){
                    NotificationController::addNotification($chat->id, 'chat', $id_user_to);
                    echo json_encode('success');
                }else{
                    echo json_encode('Error save data...');
                }
            }else{
                echo json_encode('Error input data...');
            }
        }else{
            echo json_encode('Can\'t send message...');
        }
    }

    /**
     * Fill Chat data and save
     * @param $id_user_to
     * @param $text
     * @return bool
     */
    private function filldata($id_user_to, $text)
    {
        $chat = new Chat();
        $chat->id_user_from = Yii::$app->user->id;
        $chat->id_user_to = $id_user_to;
        $chat->text = $text;
        $chat->created_at = date('U');

        if( $chat->save() )
            return $chat;
        else
            return false;
    }

    /**
     * Echos JSON-formated array of chats between current user and $id_user_to
     * @param $id_user_to
     * @throws \frontend\components\GoodException
     */
    public function actionGet_chat($id_user_to)
    {
        $id_user_from = Yii::$app->user->id;
        $arrayOfMsgs = Chat::find()
            ->where("(id_user_from = $id_user_from AND id_user_to = $id_user_to) OR 
            (id_user_to = $id_user_from AND id_user_from = $id_user_to)")
            ->orderBy('created_at')
            ->with('idUserFrom')
            ->with('idUserTo')
            ->all();

        $jsonMsgs = [];
        foreach($arrayOfMsgs as $key => $msg){
            $jsonMsgs[$key] = [
                'id_user_to'    => $msg->id_user_to,
                'email_from'    => $msg->relatedRecords['idUserFrom']->email,
                'email_to'      => $msg->relatedRecords['idUserTo']->email,
                'text'          => $msg->text,
                'created_at'    => ConvertTime::toDatetime($msg->created_at)
                ];
        }

        echo json_encode($jsonMsgs);
    }

    /**
     * Exchange files in chatting
     * @param $id_user_to
     */
    public function actionFiles($id_user_to)
    {
        $text = 'Files:';
        if( $listOfSavedFiles = FileOperations::saveFilesFromForm() ){
            foreach($listOfSavedFiles as $fileName){
                //Todo from file
                $text .= "<a href='/chatfiles/$fileName'>$fileName</a> ";
            }
            $this->filldata($id_user_to, $text);
        }
    }
}
