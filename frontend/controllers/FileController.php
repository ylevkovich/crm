<?php
namespace frontend\controllers;

use Yii;
use frontend\models\File;
use yii\base\Exception;
use frontend\components\GoodException;
use yii\web\Controller;
use app\models\UploadForm;
use yii\web\UploadedFile;
/**
 * FileController implements the CRUD actions for Files model.
 */
class FileController extends Controller
{
    /**
     * Creates directory $dirName in $inDir directory
     * @param $inDir
     * @param $dirName
     * @return string
     * @throws GoodException
     */
    public static function createDir($inDir, $dirName)
    {
        if (!is_dir($inDir))
            if (!mkdir($inDir, 0700))
                throw new GoodException('Error', 'Creating folder error...');

        $pathToDir = $inDir.'/'.$dirName;
        if (!is_dir($pathToDir))
            if (!mkdir($pathToDir, 0700))
                throw new GoodException('Error', 'Creating folder error...');
        return $pathToDir;
    }
    
    /**
     * Cleans directory
     * @param $dir string. Path to directory
     */
    public static function cleanDirectory($dir)
    {
        if ($objs = glob($dir . "/*")) {
            foreach ($objs as $obj) {
                is_dir($obj) ? self::cleanDirectory($obj) : unlink($obj);
            }
        }
    }

    /**
     * Uploads files to task's directory
     * @return string exception message if error
     * @throws GoodException if data not saved to DB
     */
    public static function uploadTasksFiles($id_task)
    {
        try{
            if( $settings = Settings::find()->one() ){
                $uploadDir = $settings->upload_dir;

                self::createDir($id_task);

                $modelUpload = new UploadForm();
                $modelUpload->file = UploadedFile::getInstances($modelUpload, 'file');
                if( $modelUpload->file && $modelUpload->validate() ){
                    foreach ($modelUpload->file as $file){
                        $pathToFile = $uploadDir . '/' .$id_task. '/' . $file->baseName . '.' . $file->extension;
                        if( !$file->saveAs($pathToFile) )
                            throw new GoodException('Error', 'Can\'t save file...');

                        if( File::find()->where([
                            'name' => $file->baseName . '.' . $file->extension,
                            'id_task' => Yii::$app->user->identity->id])->one() )
                            continue;
                        $modelFile = new File();
                        $modelFile->id_task = $id_task;
                        $modelFile->name = $file->baseName . '.' . $file->extension;
                        $modelFile->size = self::toKb($file->size);
                        if( !$modelFile->save() ){
                            unlink($pathToFile);
                            throw new GoodException('Error','Save data error...');
                        }
                    }
                }

            }else throw new GoodException('Error', 'Some problem with Settings table..');

        }catch(Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * Converts to kilobytes from bytes
     * @param $b. Bytes
     * @return mixed. Kilobytes
     */
    public static function toKb($b){
        return round($b/1024,1);
    }
    /**
     * Converts to megabytes from bytes
     * @param $b. Bytes
     * @return mixed. Megabytes
     */
    public static function toMb($b){
        return round($b/1024/1024,1);
    }

    /**
     * Returns array of files contained in task
     * @param $id_task
     * @return array
     */
    public static function getListOfFiles($id_task)
    {
        $listFileNames = [];
        if( $listFiles = File::findAll(['id_task' => $id_task]) ){
            foreach($listFiles as $file){
                $listFileNames[] = $file->name;
            }
        }
        return $listFileNames;
    }
}
