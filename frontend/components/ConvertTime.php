<?php
namespace frontend\components;
use Yii;
use yii\console\Exception;
use frontend\components\GoodException;


class ConvertTime
{
    /**
     * Converts datetime to timestamp
     * @param $datetime
     * @return int|string
     * @throws GoodException
     */
    public static function toU($datetime){
        try{
            if( !$dtime = \DateTime::createFromFormat("Y-m-d G:i", $datetime) )
                throw new GoodException('Error', 'Wrong date time format...');
            
            return $dtime->getTimestamp();
        }catch(Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * Converts timestamp to datetime
     * @param $dateStamp
     * @return string
     * @throws \app\components\GoodException
     */
    public static function toDatetime($dateStamp){
        try{
            if( is_null($dateStamp) )
                throw new GoodException('Error', 'Input time in datestamp format...');

            return date("Y-m-d G:i", $dateStamp);
        }catch(Exception $e){
            return $e->getMessage();
        }
    }

    public static function toDatetime_YmdGis($dateStamp){
        try{
            if( is_null($dateStamp) )
                throw new GoodException('Error', 'Input time in datestamp format...');

            return date("ymdGis", $dateStamp);
        }catch(Exception $e){
            return $e->getMessage();
        }
    }
}
