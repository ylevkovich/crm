<?php 
namespace frontend\components;

use yii\console\Exception;

class PostClient
{
    public $host;
    public $port;
    public $option;
    public $user;
    public $password;
    public $connect;

    function __construct($host, $port, $option, $user, $password)
    {
        $this->host = $host;
        $this->port = $port;
        $this->option = $option;
        $this->user = $user;
        $this->password = $password;

        $this->connect();
    }

    public function connect()
    {
        try{
            $this->connect = @imap_open('{'.$this->host.':'.$this->port.$this->option.'}INBOX',$this->user, $this->password);
            if(!$this->connect)
                throw new GoodException('Error', 'Can\'t connect to email client, check input data or internet connection...');
        }catch(Exception $e){
            return $e->getMessage();
        }
    }

    public function getMessages()
    {
        $countMessages = imap_num_msg($this->connect);
        if( $countMessages > 0)
            for($messageNum = 1; $messageNum <= $countMessages; $messageNum++)
                $messages[] = $this->getMessageByNum($messageNum);

        return isset($messages) ? $messages : null;
    }

    private function getMessageByNum($num)
    {
        $structure = imap_fetchstructure($this->connect, $num);
        $header = imap_header($this->connect, $num);
        $parts = array();
        $this->getParts($structure, $parts);
        if($structure->type == 1){
            $mail['body'] = imap_fetchbody($this->connect, $num, '1');

            if( $header->from[0]->host == 'gmail.com')
                $mail['body'] = $this->getPlain($mail['body']);
            else{
                $mail['body'] = imap_utf8($this->getPlain($mail['body']));
            }

            $i = 0;
            foreach($parts as $part){
                if($i > 1){
                    $file = imap_fetchbody($this->connect, $num, $i);
                    $mail['files'][] = array(
                        'content'  => base64_decode($file),
                        'filename' => $part['params'][0]['val'],
                    );
                }
                $i++;
            }
        }else{
            $mail['body'] = imap_body($this->connect, $num);
            if( $header->from[0]->host != 'gmail.com')
                $mail['body'] = imap_utf8($this->getPlain($mail['body']));
            else
                $mail['body'] = imap_utf8($this->getPlain($mail['body']));
        }

        $mail['subject'] = isset($header->subject) ? imap_utf8($header->subject) : 'empty theme';

        if (isset($header->to[0]->personal))
            $mail['to']['personal'] = imap_utf8($header->to[0]->personal);
        else
            $mail['to']['personal'] = '';
        $mail['to']['mailbox'] = imap_utf8($header->to[0]->mailbox);
        $mail['to']['host'] = imap_utf8($header->to[0]->host);

        if (isset($header->from[0]->personal))
            $mail['from']['personal'] = imap_utf8($header->from[0]->personal);
        else
            $mail['from']['personal'] = '';
        $mail['from']['mailbox'] = imap_utf8($header->from[0]->mailbox);
        $mail['from']['host'] = imap_utf8($header->from[0]->host);

        $mail['maildate'] = strtotime(imap_utf8($header->MailDate));
        $mail['date'] = strtotime(imap_utf8($header->date));
        $mail['udate'] = imap_utf8($header->udate);
        $mail['size'] = imap_utf8($header->Size);
        $mail['id'] = md5($header->message_id);

        return $mail ? $mail : null;
    }

    private function getParts($object, & $parts)
    {
        if ($object->type == 1) {
            foreach ($object->parts as $part) {
                $this->getParts($part, $parts);
            }
        } else {
            if( isset($object->disposition) && $object->disposition == 'ATTACHMENT' ){  //Converting cyrillic name of attached file
                if( !strpos( $object->parameters[0]->value, '.')) {
                    $object->parameters[0]->value = iconv( "ISO-8859-5","UTF-8",base64_decode(explode('?', $object->parameters[0]->value)[3]) );
                }
            }  //End Converting cyrillic name of attached file

            $p['type'] = $object->type;
            $p['encode'] = $object->encoding;
            $p['subtype'] = $object->subtype;
            if ($object->ifparameters == 1) {
                foreach ($object->parameters as $param) {
                    $p['params'][] = array('attr' => $param->attribute,
                        'val' => $param->value);
                }
            }
            if ($object->ifdparameters == 1) {
                foreach ($object->dparameters as $param) {
                    $p['dparams'][] = array('attr' => $param->attribute,
                        'val' => 'ISO-8859-5');
                }
            }
            $p['disp'] = null;
            if ($object->ifdisposition == 1) {
                $p['disp'] = $object->disposition;
            }
            $parts[] = $p;
        }
    }

    private function getPlain($str) //, $withoutSSL = false
    {
        if( !preg_match('/--ALT/',$str) )//without attached files
            return base64_decode($str);

        $lines = explode("\n", $str);

        if(count($lines) <= 2)
            return base64_decode($lines[0]);

        $plain = false;
        $res = '';
        $start = false;
        foreach ($lines as $line) {
            if (strpos($line, 'text/plain') !== false)
                $plain = true;
            if(strlen($line) == 1 && $plain){
                $start = true;
                $plain = false;
                continue;
            }

            if($start && strpos($line, '--') !== false)
                $start = false;
            if($start)
                $res .= $line;
        }

        return base64_decode($res);
    }

    public function deleteMessageFromMailbox($messageNum)
    {
        return imap_delete($this->connect, $messageNum);
    }
}