<?php
namespace frontend\components;
use frontend\components\GoodException;
use Yii;

class FileOperations
{
    /**
     * Opens file if that exist, else create him.
     * @param $fileName. Fully path to file
     * @return mixed resource opening file
     * @throws GoodException
     */
    public static function createOrOpenFile($fileName, $mode='c+')
    {
        //Todo concert to right charset
        if( !$src = fopen($fileName, $mode) )
            throw new GoodException('Error', 'Can\'t open file resource...');
        return $src;
    }

    /**
     * Writes $str to the file
     * @param $src. Source opening file
     * @param $str. For writing
     * @return int. Count of written symbols
     * @throws GoodException
     */
    public static function writeToFile($src, $str)
    {
        for ($written = 0; $written < strlen($str); $written += $fwrite) {
            $fwrite = fwrite($src, substr($str, $written));
            if ($fwrite === false) {
                return $written;
            }
        }
        if( !$written )
            throw new GoodException('Error', 'Can\'t write to the file...');
        return $src;
    }

    /**
     * Closes opened file resource
     * @param $src
     * @return mixed
     */
    public static function closeFile($src)
    {
        return fclose($src);
    }

    /**
     * Does all operation for writing $str to the file:
     * - creates or opens file by file name;
     * - gets content if that exist;
     * - writes in has created or has opened file $strToWrite;
     * - closes the file stream
     * @param $fileName
     * @param $strToWrite
     * @throws GoodException
     */
    public static function openOrCreateAndWrite($fileName, $strToWrite)
    {
        $src = self::createOrOpenFile($fileName);
        $fileText = file_get_contents ($fileName);
        if( $fileText == '' )
            self::writeToFile($src, $strToWrite);
        else
            self::writeToFile($src, $strToWrite.PHP_EOL.PHP_EOL.$fileText);
        self::closeFile($src);
    }

    /**
     * Opens dir if next exist, else creates it
     * @param $dir
     * @return mixed
     * @throws \frontend\components\GoodException
     */
    private static function dir($dir){
        if( !is_dir($dir)){
            if(mkdir($dir, 0777, true)){
                return $dir;
            }else{
                throw new GoodException('Error', 'Can\'t create directory - '. $dir);
            }
        }else{
            return $dir;
        }
    }

    /**
     *
     * @return array
     * @throws \frontend\components\GoodException
     */
    public static function saveFilesFromForm()
    {
        //Todo from file
        $uploaddir = self::dir('chatfiles').'/';
        $listOfSavedFiles = [];

        foreach( $_FILES['UploadForm']['name']['file'] as $key => $file ){
            $fileName = self::convertStrCyrlToLatin(basename($file));
            $uploadfile = $uploaddir.$fileName;

            if (copy($_FILES['UploadForm']['tmp_name']['file'][$key], $uploadfile)){
                $listOfSavedFiles[] = $fileName;
            }
            else {
                echo "Not upload: $uploadfile";
                exit;
            }
        }

        return $listOfSavedFiles;
    }

    /**
     * Concerts cyrillic string to latin
     * @param $fileName
     * @return mixed
     */
    private static function convertStrCyrlToLatin($fileName)
    {
        $dictionary = [
            'й' => 'i',
            'ц' => 'c',
            'у' => 'u',
            'к' => 'k',
            'е' => 'e',
            'н' => 'n',
            'г' => 'g',
            'ш' => 'sh',
            'щ' => 'shch',
            'з' => 'z',
            'х' => 'h',
            'ъ' => '',
            'ф' => 'f',
            'ы' => 'y',
            'в' => 'v',
            'а' => 'a',
            'п' => 'p',
            'р' => 'r',
            'о' => 'o',
            'л' => 'l',
            'д' => 'd',
            'ж' => 'zh',
            'э' => 'e',
            'ё' => 'e',
            'я' => 'ya',
            'ч' => 'ch',
            'с' => 's',
            'м' => 'm',
            'и' => 'i',
            'т' => 't',
            'ь' => '',
            'б' => 'b',
            'ю' => 'yu',

            'Й' => 'I',
            'Ц' => 'C',
            'У' => 'U',
            'К' => 'K',
            'Е' => 'E',
            'Н' => 'N',
            'Г' => 'G',
            'Ш' => 'SH',
            'Щ' => 'SHCH',
            'З' => 'Z',
            'Х' => 'X',
            'Ъ' => '',
            'Ф' => 'F',
            'Ы' => 'Y',
            'В' => 'V',
            'А' => 'A',
            'П' => 'P',
            'Р' => 'R',
            'О' => 'O',
            'Л' => 'L',
            'Д' => 'D',
            'Ж' => 'ZH',
            'Э' => 'E',
            'Ё' => 'E',
            'Я' => 'YA',
            'Ч' => 'CH',
            'С' => 'S',
            'М' => 'M',
            'И' => 'I',
            'Т' => 'T',
            'Ь' => '',
            'Б' => 'B',
            'Ю' => 'YU',

            ' ' => '__',
            '\-' => '-',
            '\s' => '-',

            '[^a-zA-Z0-9\-]' => '',

            '[-]{2,}' => '-',
        ];

        return strtr($fileName,$dictionary);
    }
}