<?php
namespace frontend\components;

use Yii;
use frontend\controllers\FileController;

class Email
{
    /**
     * Sends text email by using SMTP
     * @param string $textBody
     * @param string $to
     * @param string $subject
     * @param string $from
     * @return bool
     * @throws GoodException
     */
    public static function send(
        $textBody = '',
        $to = '',
        $subject = '',
        $from = 'from@domain.com'){

        if( $to != '' ){
            $message = Yii::$app->mail->compose();
            $message->setFrom($from);
            $message->setTo($to);
            $message->setSubject($subject);
            $message->setTextBody($textBody);

            if( !empty($_FILES['UploadForm']['name']['file'][0]) ){
                foreach( $_FILES['UploadForm']['name']['file'] as $key => $file ){
                    $uploadfile = 'temp/'.basename($file);
                    copy($_FILES['UploadForm']['tmp_name']['file'][$key], $uploadfile);
                    $message->attach( $uploadfile );
                }
            }

            if( $message->send() ){
                FileController::cleanDirectory('temp');
                return true;
            }

            return false;
        }else throw new GoodException('Error', 'Input recipient');
    }

    /**
     * Sends html email by using SMTP
     * @param string $htmlBody
     * @param string $to
     * @param string $subject
     * @param string $from
     * @return bool
     * @throws GoodException
     */
    public static function sendHtml(
        $htmlBody = '',
        $to = '',
        $subject = '',
        $from = 'from@domain.com'){

        if( $to != '' ){
            $message = Yii::$app->mail->compose();
            $message->setFrom($from);
            $message->setTo($to);
            $message->setSubject($subject);
            $message->setHtmlBody($htmlBody);

            if( !empty($_FILES['UploadForm']['name']['file'][0]) ){
                foreach( $_FILES['UploadForm']['name']['file'] as $key => $file ){
                    $uploadfile = 'temp/'.basename($file);
                    copy($_FILES['UploadForm']['tmp_name']['file'][$key], $uploadfile);
                    $message->attach( $uploadfile );
                }
            }

            if( $message->send() ){
                FileController::cleanDirectory('temp');
                return true;
            }

            return false;
        }else throw new GoodException('Error', 'Input recipient');
    }
}
