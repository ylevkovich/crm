<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "chat".
 *
 * @property integer $id
 * @property integer $id_user_from
 * @property integer $id_user_to
 * @property string $text
 * @property integer $created_at
 *
 * @property User $idUserFrom
 * @property User $idUserTo
 */
class Chat extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user_from', 'id_user_to', 'text', 'created_at'], 'required'],
            [['id_user_from', 'id_user_to', 'created_at'], 'integer'],
            [['text'], 'string'],
            [['id_user_from'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user_from' => 'id']],
            [['id_user_to'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user_to' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user_from' => 'Id User From',
            'id_user_to' => 'Id User To',
            'text' => 'Text',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUserFrom()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user_from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUserTo()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user_to']);
    }
}
