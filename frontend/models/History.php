<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "history".
 *
 * @property integer $id
 * @property integer $id_task
 * @property integer $id_user
 * @property integer $action
 * @property integer $created
 *
 * @property Task $idTask
 */
class History extends \yii\db\ActiveRecord
{
    public static $actions = [
        'create' => 0,
        'appoint' => 1,
        'abandon' => 2,
        'elaborationQuestion' => 3,
        'elaborationAnswer' => 4,
        'closingTask' => 5,
    ];

    public static $actionsTranslate = [
        0 => 'создание',
        1 => 'назначил исполнителя',
        2 => 'отказался от задачи',
        3 => 'уточнение: вопрос',
        4 => 'уточнение: ответ',
        5 => 'закрытие задачи',
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_task', 'created'], 'required'],
            [['id_task', 'action', 'created'], 'integer'],
            [['id_task'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['id_task' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_task' => 'Id Task',
            'action' => 'Action',
            'created' => 'Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), [ 'id'=> 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), [ 'id'=> 'id_task']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'id_task']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}
