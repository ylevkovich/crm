<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property integer $created
 * @property integer $id_user
 * @property integer $status
 * @property string $theme
 * @property string $text
 * @property integer $id_mainTask
 *
 * @property Task $idMainTask
 * @property Task[] $tasks
 * @property User $idUser
 * @property integer isReadingNow
 * @property integer lastReadTime
     * @property integer appointed
 * @property integer position
 */
class Task extends \yii\db\ActiveRecord
{
    public static $status = [
        'new' => 0,
        'reading' => 1,
        'overdue' => 2,
        'appointed' => 3,
        'inElaboration' => 4,
        'closed' => 5
    ];

    public static $statusTranslate = [
        0 => ['rusName'=>'новая','color'=> ''],
        1 => ['rusName'=>'чтение','color'=> 'green'],
        2 => ['rusName'=>'прострочена','color'=> 'red'],
        3 => ['rusName'=>'назначена','color'=> ''],
        4 => ['rusName'=>'в уточнении','color'=> ''],
        5 => ['rusName'=>'закрыта','color'=> ''],
    ];
    
    public static function tableName()
    {
        return 'task';
    }

    public function rules()
    {
        return [
            [['created', 'theme', 'status'], 'required'],
            [['created', 'id_user', 'status', 'id_mainTask', 'isReadingNow', 'lastReadTime'], 'integer'],
            [['theme', 'text'], 'string'],
            [['id_mainTask'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['id_mainTask' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
            [['isReadingNow'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['isReadingNow' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created' => 'Created',
            'id_user' => 'Id User',
            'status' => 'Status',
            'theme' => 'Theme',
            'text' => 'Text',
            'id_mainTask' => 'Id Main Task',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMainTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'id_mainTask']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['id_mainTask' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), [ 'id'=> 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), [ 'id_task'=> 'id']);
    }
    
    public function getUserEmail()
    {
        return $this->user->email;
    }

    public function getUserReading()
    {
        return $this->hasOne(User::className(), [ 'id'=> 'isReadingNow']);
    }

    public function getUserReadingEmail()
    {
        
        if( !is_null($this->userReading) )
            return $this->userReading->email;
        return null;
    }
}
