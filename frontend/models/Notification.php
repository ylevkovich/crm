<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "notification".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $id_elaboration_or_chat
 * @property integer $id_user_to
 */
class Notification extends \yii\db\ActiveRecord
{
    public static $type = [
        'chat' => 0,
        'elaboration' => 1,
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'id_elaboration_or_chat'], 'required'],
            [['type', 'id_elaboration_or_chat'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'id_elaboration_or_chat' => 'Id Elaboration Or Chat',
        ];
    }
}
