<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property integer $id
 * @property string $upload_dir
 * @property integer $reactionTime
 * @property integer $operation_day_from
 * @property integer $operation_day_to
 * @property string $inputPost_password
 * @property string $inputPost_port
 * @property string $inputPost_email
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['upload_dir', 'reactionTime', 'operation_day_from', 'operation_day_to', 'inputPost_password', 'inputPost_email'], 'required'],
            [['upload_dir', 'inputPost_password', 'inputPost_port', 'inputPost_email'], 'string'],
            [['reactionTime', 'operation_day_from', 'operation_day_to'], 'integer'],
            [['inputPost_email'], 'unique'],
            [['inputPost_port'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'upload_dir' => 'Папка с файлами',
            'reactionTime' => 'Время реакции в секундах',
            'operation_day_from' => 'Операционный день с - Н: 9:00 - 900',
            'operation_day_to' => 'Операционный день до - Н: 9:00 - 900',
            'inputPost_login' => 'Input Post Login',
            'outputPost_login' => 'Output Post Login',
            'inputPost_password' => 'Input Post Password',
            'outputPost_password' => 'Output Post Password',
            'inputPost_port' => 'Input Post Port',
            'outputPost_port' => 'Output Post Port',
            'inputPost_email' => 'Input Post Email',
            'outputPost_email' => 'Output Post Email',
        ];
    }
}
