<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "Elaboration".
 *
 * @property integer $id
 * @property integer $id_task
 * @property integer $type
 * @property integer $id_author
 * @property integer $created
 * @property string $text
 */
class Elaboration extends \yii\db\ActiveRecord
{
    public static $type = [
        'question' => 0,
        'answer' => 1,
        'closing' => 2
    ];

    public static $typeTranslate = [
        0 => 'вопрос',
        1 => 'ответ',
        2 => 'закрытие'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Elaboration';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_task', 'type', 'id_author', 'created'], 'required'],
            [['id_task', 'type', 'id_author', 'created'], 'integer'],
            [['text'], 'string'],
            [['id_task'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['id_task' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_task' => 'Id Task',
            'type' => 'Type',
            'id_author' => 'Id Author',
            'created' => 'Created',
            'text' => 'Text',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), [ 'id'=> 'id_author']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), [ 'id'=> 'id_task']);
    }
}
