<?php

use yii\db\Migration;

/**
 * Handles adding coloumn_lastReadTime to table `task`.
 */
class m160722_074715_add_coloumn_lastReadTime_to_task extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('task', 'lastReadTime', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('task', 'lastReadTime');
    }
}
