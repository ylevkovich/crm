<?php

use yii\db\Migration;

class m160819_111114_add_field_last_activity_to_table_user extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'last_activity', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('user', 'last_activity');
    }
}
