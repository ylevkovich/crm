<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_elaboration`.
 */
class m160808_070029_create_table_elaboration extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('elaboration', [
            'id' => $this->primaryKey(),
            'id_task' => $this->integer()->notNull(),
            'type' => $this->smallInteger()->notNull(),
            'id_author' => $this->integer()->notNull(),
            'created' => $this->integer()->notNull(),
            'text' => $this->text()
        ]);

        $this->addForeignKey('elaboration_task', 'elaboration', 'id_task', 'task', 'id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('elaboration');
    }
}
