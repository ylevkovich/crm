<?php

use yii\db\Migration;

/**
 * Handles the creation for table `files`.
 */
class m160623_100044_create_files extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('files', [
            'id' => $this->primaryKey(),
            'id_task' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'size' => $this->double()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('id_task_task', 'files', 'id_task', 'task', 'id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('files');
    }
}
