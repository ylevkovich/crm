<?php

use yii\db\Migration;

class m160812_082948_history extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('history', [
            'id' => $this->primaryKey(),
            'id_task' => $this->integer()->notNull(),
            'action' => $this->smallInteger()->notNull()->defaultValue(0),
            'created' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('history_task', 'history', 'id_task', 'task', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('history');
    }
}
