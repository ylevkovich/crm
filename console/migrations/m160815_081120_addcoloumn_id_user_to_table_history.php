<?php

use yii\db\Migration;

class m160815_081120_addcoloumn_id_user_to_table_history extends Migration
{
    public function safeUp()
    {
        $this->addColumn('history', 'id_user', $this->integer()->notNull()->defaultValue(2));

        $this->addForeignKey('history_user', 'history', 'id_user', 'user', 'id');
    }

    public function safeDown()
    {
        $this->dropColumn('history', 'id_user');
    }
}
