<?php

use yii\db\Migration;

class m160711_135943_edit_table_task extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('task','deadline');
        $this->dropColumn('task','email');
    }

    public function safeDown()
    {
        $this->addColumn('task', 'deadline', $this->integer());
        $this->addColumn('task', 'email', $this->string(32));
    }
}
