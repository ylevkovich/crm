<?php

use yii\db\Migration;

class m160823_142836_create_table_settings extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%settings}}', [
            'id' => $this->primaryKey(),
            'upload_dir' => $this->string(255)->notNull(),
            'reactionTime' => $this->integer()->notNull(),
            'operation_day_from' => $this->integer()->notNull(),
            'operation_day_to' => $this->integer()->notNull(),
            'inputPost_password' => $this->string()->notNull(),
            'inputPost_port' => $this->string()->unique(),
            'inputPost_email' => $this->string()->notNull()->unique(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%settings}}');
    }
}
