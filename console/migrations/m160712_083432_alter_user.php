<?php

use yii\db\Migration;

class m160712_083432_alter_user extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('user','username',$this->string());
    }

    public function safeDown()
    {
        $this->alterColumn('user','username', $this->string()->notNull()->unique());
    }
}
