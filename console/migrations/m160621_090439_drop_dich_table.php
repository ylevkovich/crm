<?php

use yii\db\Migration;

/**
 * Handles the dropping for table `dich_table`.
 */
class m160621_090439_drop_dich_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropTable('dich_table');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->createTable('dich_table', [
            'id' => $this->primaryKey(),
        ]);
    }
}
