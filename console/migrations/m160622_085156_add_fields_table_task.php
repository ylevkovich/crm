<?php

use yii\db\Migration;

class m160622_085156_add_fields_table_task extends Migration
{

    public function safeUp()
    {
        $this->addColumn('task', 'deadline', $this->integer());
        $this->addColumn('task', 'email', $this->string(32));
        $this->addColumn('task', 'id_mainTask', $this->integer());

        $this->addForeignKey('mainTask_task', 'task', 'id_mainTask', 'task', 'id');
    }

    public function safeDown()
    {
        $this->dropColumn('task', 'deadline');
        $this->dropColumn('task', 'email');
        $this->dropColumn('task', 'id_mainTask');
    }
}
