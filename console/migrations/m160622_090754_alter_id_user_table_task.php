<?php

use yii\db\Migration;

class m160622_090754_alter_id_user_table_task extends Migration
{
    public function up()
    {
        $this->alterColumn('task', 'id_user', $this->integer());
    }

    public function down()
    {
        $this->alterColumn('task', 'id_user', $this->integer()->notNull());
    }
}
