<?php

use yii\db\Migration;

/**
 * Handles adding coloumn_appointed to table `table_task`.
 */
class m160728_150403_add_coloumn_appointed_to_table_task extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('task','appointed', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('task','appointed');
    }
}
