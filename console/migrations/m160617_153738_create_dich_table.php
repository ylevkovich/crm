<?php

use yii\db\Migration;

/**
 * Handles the creation for table `dich_table`.
 */
class m160617_153738_create_dich_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('dich_table', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('dich_table');
    }
}
