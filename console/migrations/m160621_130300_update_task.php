<?php

use yii\db\Migration;

class m160621_130300_update_task extends Migration
{
    public function up()
    {
        $this->renameColumn('task', 'datatime', 'created');
    }

    public function down()
    {
        $this->renameColumn('task', 'created', 'datatime');
    }
}
