<?php

use yii\db\Migration;

/**
 * Handles the dropping for table `coloumn_in_task`.
 */
class m160801_074021_drop_coloumn_in_task extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('files', 'size');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('files', 'size', $this->double()->notNull());
    }
}
