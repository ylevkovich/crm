<?php

use yii\db\Migration;

class m160621_145313_alter_column_table_task extends Migration
{
    public function safeUp()
    {
        $this->dropTable('task');

        $this->createTable('task', [
            'id' => $this->primaryKey(),
            'created' => $this->integer()->notNull(),
            'id_user' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'theme' => $this->string(255)->notNull(),
            'text' => $this->text()
        ]);

        $this->addForeignKey('task_user', 'task', 'id_user', 'user', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('task');

        $this->createTable('task', [
            'id' => $this->primaryKey(),
            'created' => $this->dateTime(),
            'id_user' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'theme' => $this->string(255)->notNull(),
            'text' => $this->text()
        ]);

        $this->addForeignKey('task_user', 'task', 'id_user', 'user', 'id');
    }
}
