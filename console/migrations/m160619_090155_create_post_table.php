<?php

use yii\db\Migration;

/**
 * Handles the creation for table `post_table`.
 */
class m160619_090155_create_post_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('post_table', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('post_table');
    }
}
