<?php

use yii\db\Migration;

/**
 * Handles the creation for table `task`.
 */
class m160621_114937_create_task extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('task', [
            'id' => $this->primaryKey(),
            'datatime' => $this->dateTime(),
            'id_user' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'theme' => $this->string(255)->notNull(),
            'text' => $this->text()
        ]);

        $this->addForeignKey('task_user', 'task', 'id_user', 'user', 'id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('task');
    }
}
