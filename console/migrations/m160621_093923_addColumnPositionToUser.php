<?php

use yii\db\Migration;

class m160621_093923_addColumnPositionToUser extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}','position', $this->string(32)->notNull());
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'position');
    }
}
