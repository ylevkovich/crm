<?php

use yii\db\Migration;

/**
 * Handles adding coloumn_isReadingNow to table `task`.
 */
class m160721_055659_add_coloumn_isReadingNow_to_task extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('task', 'isReadingNow', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('task', 'isReadingNow');
    }
}
