<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_chat`.
 */
class m160629_090650_create_table_chat extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('chat', [
            'id' => $this->primaryKey(),
            'id_user_from' => $this->integer()->notNull(),
            'id_user_to' => $this->integer()->notNull(),
            'text' => $this->text()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('id_user_from', 'chat', 'id_user_from', 'user', 'id');

        $this->addForeignKey('id_user_to', 'chat', 'id_user_to', 'user', 'id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('chat');
    }
}
