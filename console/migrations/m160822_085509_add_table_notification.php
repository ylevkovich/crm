<?php

use yii\db\Migration;

class m160822_085509_add_table_notification extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('notification', [
            'id' => $this->primaryKey(),
            'type' => $this->smallInteger()->notNull(),
            'id_elaboration_or_chat' => $this->integer()->notNull(),
            'id_user_to' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('notification_user', 'notification', 'id_user_to', 'user', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('notification');
    }
}
