<?php

use yii\db\Migration;

class m160621_094534_dropTablesUser_tableAndPost_table extends Migration
{

    public function safeUp()
    {
        $this->dropTable('post_table');
        $this->dropTable('user_table');
    }

    public function safeDown()
    {
        return false;
    }
}
